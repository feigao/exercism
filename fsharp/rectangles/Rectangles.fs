﻿module Rectangles

let flip f a b = f b a

let rectangles lines =
    let cells =
        lines
        |> List.mapi (fun r line ->
               line
               |> Seq.toList
               |> List.mapi (fun c ch -> ((r, c), ch)))
        |> List.concat
        |> Map.ofList

    let joints =
        cells
        |> Map.toList
        |> List.filter (snd >> ((=) '+'))
        |> List.map fst

    let getRows rcs =
        rcs
        |> List.map fst
        |> List.distinct
        |> List.sort

    let getPairs xs =
        [ for r1 in xs do
              for r2 in xs do
                  if r1 < r2 then yield (r1, r2) ]

    let findCols rcs (r1, r2) =
        let c1 =
            rcs
            |> List.filter (fst >> (=) r1)
            |> List.map snd
            |> Set.ofList

        let c2 =
            rcs
            |> List.filter (fst >> (=) r2)
            |> List.map snd
            |> Set.ofList

        Set.intersect c1 c2 |> Set.toList

    let checkRows ((r1, r2), (c1, c2)) table =
        [ c1..c2 ]
        |> List.collect (fun c ->
               [ (r1, c)
                 (r2, c) ])
        |> List.map (flip Map.find table)
        |> List.forall (fun c -> c = '+' || c = '-')

    let checkCols ((r1, r2), (c1, c2)) table =
        [ r1..r2 ]
        |> List.collect (fun r ->
               [ (r, c1)
                 (r, c2) ])
        |> List.map (flip Map.find table)
        |> List.forall (fun c -> c = '+' || c = '|')

    let rows = joints |> getRows
    let rowPairs = getPairs rows |> List.filter (fun (x, y) -> x < y)
    rowPairs
    |> List.collect (fun (r1, r2) ->
           findCols joints (r1, r2)
           |> getPairs
           |> List.map (fun (c1, c2) -> ((r1, r2), (c1, c2))))
    |> List.filter
           (fun rc ->
           [ checkRows; checkCols ] |> List.forall (fun check -> check rc cells))
    |> List.length
