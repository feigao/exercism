﻿module Transpose

let transpose (rows : string list) =
    let placeholder = char 0
    rows
    |> List.map (List.ofSeq
                 >> (fun cs ->
                 cs @ List.init ((rows
                                  |> List.map String.length
                                  |> List.max)
                                 - List.length cs) (fun _ -> placeholder)))
    |> List.transpose
    |> List.map (fun cs ->
           cs
           |> List.rev
           |> List.skipWhile (fun c -> c = placeholder)
           |> List.map (fun c ->
                  if c = placeholder then ' '
                  else c)
           |> List.rev
           |> System.String.Concat)
