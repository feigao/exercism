﻿module RunLengthEncoding

open System

let encode (input : string) =
    input
    |> List.ofSeq
    |> List.fold (fun acc c ->
           match acc with
           | [] -> [ (c, 1) ]
           | (x, xi) :: rest ->
               if x = c then (x, xi + 1) :: rest
               else (c, 1) :: acc) []
    |> List.rev
    |> List.map (fun (c, l) ->
           if l = 1 then ""
           else string l
           + string c)
    |> String.Concat

let decode (input : string) =
    input
    |> List.ofSeq
    |> List.rev
    |> List.fold (fun acc c ->
           if Char.IsDigit c then
               match acc with
               | (x, xi) :: rest -> (x, c :: xi) :: rest
               | [] -> failwith ""
           else (c, []) :: acc) []
    |> List.map (fun (c, ds) ->
           match ds with
           | [] -> string c
           | ds ->
               String.replicate (ds
                                 |> String.Concat
                                 |> int) (string c))
    |> String.Concat
