﻿module BinarySearchTree

type Tree<'a> =
    | Node of 'a * 'a Tree * 'a Tree
    | Empty

let rec insert (value : 'a) (node : Tree<'a>) : Tree<'a> =
    match node with
    | Empty -> Node(value, Empty, Empty)
    | Node(v, l, r) ->
        if value <= v then Node(v, insert value l, r)
        else Node(v, l, insert value r)

let flip f a b = f b a
let create (items : 'a list) : Tree<'a> = List.fold (flip insert) Empty items

let left (node : Tree<'a>) : Tree<'a> option =
    match node with
    | Empty -> None
    | Node(_, Empty, _) -> None
    | Node(_, l, _) -> Some l

let right (node : Tree<'a>) : Tree<'a> option =
    match node with
    | Empty -> None
    | Node(_, _, Empty) -> None
    | Node(_, _, r) -> Some r

let data (node : Tree<'a>) =
    match node with
    | Empty -> failwith "empty tree"
    | Node(v, _, _) -> v

let rec sortedData (node : Tree<'a>) : 'a list =
    match node with
    | Empty -> []
    | Node(v, l, r) -> (sortedData l) @ [ v ] @ (sortedData r)
