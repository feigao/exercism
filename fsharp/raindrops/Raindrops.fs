﻿module Raindrops

let multipleOf d n = n % d = 0

let numberToString : ((int -> bool) * string) list =
    [ (multipleOf 3, "Pling")
      (multipleOf 5, "Plang")
      (multipleOf 7, "Plong") ]

let convert (number : int) : string =
    let s =
        numberToString
        |> List.fold (fun acc (p, s) ->
               if p number then acc + s
               else acc) "" // string option list
    if s = "" then string number
    else s
