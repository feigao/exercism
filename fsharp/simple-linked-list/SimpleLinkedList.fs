module SimpleLinkedList
open System.Collections.Generic

type LinkedList<'a> =
    | Nil
    | Cons of 'a * LinkedList<'a>

let nil = Nil

let create x n = Cons (x, n)

let isNil x = x = Nil

let next = function
    | Nil -> failwith "Empty List"
    | Cons (_, n) -> n

let datum = function
    | Cons (v, _) -> v
    | Nil -> failwith "Empty List"

let rec toList = function
    | Nil -> []
    | Cons (v, n) -> v :: toList n

let rec fromList =
    function
    | [] -> Nil
    | x :: xs -> Cons (x , fromList xs)

let flip f a b = f b a

let rec fold (func : 'b -> 'a -> 'b) (state : 'b) = function
    | Nil -> state
    | Cons (v, n) -> fold func (func state v) n

let reverse (list: LinkedList<'a>) : LinkedList<'a> =
    fold (flip create) Nil list
