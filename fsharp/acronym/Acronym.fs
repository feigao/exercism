﻿module Acronym

open System.Text.RegularExpressions
open System

let abbreviate (phrase : string) : string =
    Regex.Matches((phrase.ToUpper()), @"[a-zA-Z]+")
    |> Seq.map (fun gp -> gp.Value.Chars 0)
    |> String.Concat
