﻿module Anagram

open System

let findAnagrams (sources : string list) (target : string) =
    let trans : string -> char array =
        Seq.map (Char.ToLower)
        >> Seq.sort
        >> Seq.toArray

    let t = trans target
    sources
    |> List.filter (trans >> (=) t)
    |> List.filter (String.map Char.ToLower >> (<>) (target.ToLower()))
