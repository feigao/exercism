﻿module KindergartenGarden

type Plant =
    | Radishes
    | Clover
    | Grass
    | Violets

let charToPlant : Map<char, Plant> =
    [ ('R', Radishes)
      ('C', Clover)
      ('G', Grass)
      ('V', Violets) ]
    |> Map.ofList

let rec split (sep : char) (ss : char list) : char list list =
    match List.tryFindIndex ((=) sep) ss with
    | None -> [ ss ]
    | Some i -> List.take i ss :: split sep (List.skip (i + 1) ss)

let rec transpose (xss : 'a list list) =
    if List.forall List.isEmpty xss then []
    else List.map List.head xss :: transpose (List.map List.tail xss)

let plants (diagram : string) (student : string) : Plant list =
    diagram
    |> Seq.toList
    |> split '\n'
    |> List.map (List.chunkBySize 2)
    |> transpose
    |> List.map List.concat
    |> List.item (int student.[0] - int 'A')
    |> List.map (fun ch -> Map.find ch charToPlant)
