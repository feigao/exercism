﻿module IsbnVerifier

let isValid (isbn : string) =
    let clean =
        isbn
        |> Seq.toList
        |> List.filter ((<>) '-')
        |> List.rev
    if List.length clean <> 10 then false
    elif List.exists (fun c -> c <> 'X' && not (System.Char.IsDigit c)) clean then
        false
    else if clean
            |> List.tail
            |> List.forall System.Char.IsDigit
            |> not
    then false
    else
        clean
        |> List.map (function
               | 'X' -> 10
               | c ->
                   c
                   |> string
                   |> int)
        |> List.zip [ 1..10 ]
        |> List.sumBy (fun (x, y) -> x * y)
        |> (fun x -> x % 11 = 0)
