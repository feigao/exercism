﻿module Seq

let rec keep pred xs =
    seq {
        for x in xs do
            if pred x then yield x
    }

let rec discard pred xs = Seq.filter (pred >> not) xs
