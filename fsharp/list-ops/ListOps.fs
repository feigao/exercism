﻿module ListOps

let rec foldl folder state list =
    match list with
    | [] -> state
    | x :: xs -> foldl folder (folder state x) xs

let rec foldr folder state list =
    match list with
    | [] -> state
    | x :: xs -> folder x (foldr folder state xs)

let length list = foldr (fun _ x -> x + 1) 0 list
let reverse list = foldl (fun acc x -> x :: acc) [] list
let map f list = foldr (fun x acc -> f x :: acc) [] list

let filter f list =
    foldr (fun x acc ->
        if f x then x :: acc
        else acc) [] list

let append xs ys = foldr (fun x acc -> x :: acc) ys xs
let concat xs = foldr append [] xs
