﻿module PrimeFactors

let rec aux number (d, acc) =
    if number = 1L then (d, acc)
    elif d * d > number then (number, number :: acc)
    elif number % d = 0L then aux (number / d) (d, d :: acc)
    else aux number (d + 1L, acc)

let rec factors number =
    aux number (2L, [])
    |> snd
    |> List.sort
    |> List.map int
