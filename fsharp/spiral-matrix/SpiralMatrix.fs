﻿module SpiralMatrix

let rotateCounterClockwise = List.transpose >> List.map List.rev

let rec subMatrix n sizeX sizeY =
    match sizeX, sizeY with
    | 0, _ -> []
    | _, 0 -> []
    | _ ->
        [ n..(n + sizeX - 1) ]
        :: (subMatrix (n + sizeX) (sizeY - 1) sizeX |> rotateCounterClockwise)

let spiralMatrix size = subMatrix 1 size size
