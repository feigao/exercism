﻿module Hamming

let distance (strand1: string) (strand2: string): int option =
    if String.length strand1 <> String.length strand2
    then None
    else
        Array.map2
            (fun x y -> if x = y then 0 else 1)
            (strand1.ToCharArray())
            (strand2.ToCharArray())
        |> Array.sum
        |> Some
