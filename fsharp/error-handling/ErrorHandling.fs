﻿module ErrorHandling

open System

let handleErrorByThrowingException() = failwith "error"

let handleErrorByReturningOption (input : string) =
    match Int32.TryParse input with
    | true, v -> Some v
    | _ -> None

let handleErrorByReturningResult (input : string) =
    match Int32.TryParse input with
    | true, v -> Ok v
    | _ -> Error "Could not convert input to integer"

let bind switchFunction twoTrackInput = Result.bind switchFunction twoTrackInput

let cleanupDisposablesWhenThrowingException (resource : IDisposable) : unit =
    try
        failwith "some error"
    finally
        resource.Dispose()
