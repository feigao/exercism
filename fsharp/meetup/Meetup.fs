﻿module Meetup

open System

type Week = First | Second | Third | Fourth | Last | Teenth

let meetup year month week dayOfWeek: DateTime =
    [1..DateTime.DaysInMonth (year, month)]
    |> List.map (fun day -> DateTime (year,month,day))
    |> List.filter (fun dt -> dt.DayOfWeek = dayOfWeek)
    |> ( fun days ->
        match week with
        | First -> List.item 0 days
        | Second -> List.item 1 days
        | Third -> List.item 2 days
        | Fourth -> List.item 3 days
        | Last -> List.last days
        | Teenth -> List.find (fun d -> 13 <= d.Day && d.Day <= 19) days
    )