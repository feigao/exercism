﻿module WordCount

open System

module Map =
    let updateValueWithDefault key func defaultValue map =
        match Map.tryFind key map with
        | None -> Map.add key defaultValue map
        | Some v -> Map.add key (func v) map

let countWords phrase =
    let sent = phrase |> String.map System.Char.ToLower
    Text.RegularExpressions.Regex.Matches(sent, "([a-z]+'[a-z]+|[a-z]+|[0-9]+)")
    |> Seq.map (fun m -> m.Value)
    |> Seq.fold (fun map w -> Map.updateValueWithDefault w ((+) 1) 1 map)
           Map.empty
