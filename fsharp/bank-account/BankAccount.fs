﻿module BankAccount

type AccountStatus =
    | Pending
    | Opened
    | Closed

type Account =
    { mutable Balance : decimal
      mutable Status : AccountStatus }

let mkBankAccount() =
    { Balance = 0.0m
      Status = Pending }

let openAccount account =
    lock account (fun () ->
        account.Status <- Opened
        account)

let getBalance account =
    lock account (fun () ->
        match account with
        | { Status = Opened } -> Some account.Balance
        | _ -> None)

let closeAccount account =
    lock account (fun () ->
        account.Status <- Closed
        account)

let updateBalance change account =
    lock account (fun () ->
        match account with
        | { Status = Opened } ->
            account.Balance <- account.Balance + change
            account
        | _ -> failwith "account not open")
