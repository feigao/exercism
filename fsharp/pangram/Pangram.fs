﻿module Pangram

open System

let isPangram (input : string) : bool =
    input.ToLower().ToCharArray()
    |> Array.filter (Char.IsLetter)
    |> Array.groupBy id
    |> Array.length
    |> ((=) 26)
