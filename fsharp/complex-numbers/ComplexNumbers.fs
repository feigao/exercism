﻿module ComplexNumbers

type Complex =
    { Real : float
      Imaginary : float }

let create real imaginary =
    { Real = real
      Imaginary = imaginary }

let mul z1 z2 =
    { Real = z1.Real * z2.Real - z1.Imaginary * z2.Imaginary
      Imaginary = z1.Imaginary * z2.Real + z1.Real * z2.Imaginary }

let add z1 z2 =
    { Real = z1.Real + z2.Real
      Imaginary = z1.Imaginary + z2.Imaginary }

let sub z1 z2 =
    { Real = z1.Real - z2.Real
      Imaginary = z1.Imaginary - z2.Imaginary }

let conjugate z =
    { Real = z.Real
      Imaginary = -z.Imaginary }

let real z = z.Real
let imaginary z = z.Imaginary
let abs z = (z.Real * z.Real + z.Imaginary * z.Imaginary) |> System.Math.Sqrt
let exp z =
    mul (create (System.Math.Exp z.Real) 0.)
        (create (System.Math.Cos z.Imaginary) (System.Math.Sin z.Imaginary))
let reciprocal z = mul (conjugate z) (create (1.0 / (abs z) ** 2.) 0.)
let div z1 z2 = mul z1 (reciprocal z2)
