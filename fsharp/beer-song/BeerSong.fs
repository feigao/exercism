﻿module BeerSong

let sWall n = sprintf "%d bottles of beer on the wall, %d bottles of beer." n n
let sTake n =
    sprintf "Take one down and pass it around, %d bottle%s of beer on the wall."
        (n - 1) (if n - 1 > 1 then "s"
                 else "")

let rec interleave (sep : 'a) (ls : 'a list) =
    match ls with
    | []
    | [ _ ] -> ls
    | x :: ys -> x :: sep :: interleave sep ys

let recite (startBottles : int) (takeDown : int) : string list =
    let take (start : int) : string list =
        match start with
        | 0 ->
            [ "No more bottles of beer on the wall, no more bottles of beer.";
              "Go to the store and buy some more, 99 bottles of beer on the wall." ]
        | 1 ->
            [ "1 bottle of beer on the wall, 1 bottle of beer.";
              "Take it down and pass it around, no more bottles of beer on the wall." ]
        | n ->
            [ sWall n
              sTake n ]

    let down = min (startBottles + 1) takeDown
    List.init down (fun i -> take (startBottles - i)) // string list list
    |> interleave [ "" ]
    |> List.concat
