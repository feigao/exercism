﻿module Markdown

open System.Text.RegularExpressions

let tag t text = sprintf "<%s>%s</%s>" t text t
let strong (line : string) : string =
    Regex.Replace(line, @"__(.+?)__", @"<strong>$1</strong>")
let em (line : string) = Regex.Replace(line, @"_(.+?)_", @"<em>$1</em>")

let head (line : string) =
    let matches = Regex.Match(line, @"^(#+)\s+(.*)$").Groups
    tag ("h" + string matches.[1].Length) matches.[2].Value

let li (line : string) = Regex.Replace(line, @"^\* (.*)$", @"<li>$1</li>")
let p (line : string) =
    tag "p" (line
             |> strong
             |> em)
let ul (line : string) = tag "ul" line

let (|Li|_|) (line : string) =
    if line.StartsWith("* ") then Some Li
    else None

let (|Head|_|) (line : string) =
    if line.StartsWith("#") then Some Head
    else None

let rec parse (markdown : string) =
    markdown.Split('\n')
    |> Array.map (fun line ->
           match line with
           | Li ->
               li
               >> strong
               >> em
               >> ul
           | Head -> head
           | _ -> p
           <| line)
    |> String.concat ""
    |> fun s -> s.Replace("</ul><ul>", "")
