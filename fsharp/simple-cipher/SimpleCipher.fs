﻿module SimpleCipher

open System

let charToIndex c = int c - int 'a'
let indexToChar i = ((i % 26 + 26) % 26) + int 'a' |> char

let unshift origin offset =
    origin
    |> charToIndex
    |> fun x -> x - (offset |> charToIndex)
    |> indexToChar

let shift origin offset =
    origin
    |> charToIndex
    |> (+) (offset |> charToIndex)
    |> indexToChar

type SimpleCipher(key : string) =
    member this.Key = key

    member this.Encode(plaintext) =
        let count = String.length plaintext / String.length this.Key
        let key = String.replicate (count + 1) this.Key
        Seq.map2 shift plaintext key |> String.Concat

    member this.Decode(ciphertext) =
        let count = String.length ciphertext / String.length this.Key
        let key = String.replicate (count + 1) this.Key
        Seq.map2 unshift ciphertext key |> String.Concat

    new() =
        let rng = Random()
        let key =
            String.init 128 (fun _ ->
                rng.Next 26 + 97
                |> char
                |> string)
        SimpleCipher(key)
