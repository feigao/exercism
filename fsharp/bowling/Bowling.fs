module Bowling

type Game = int list list

let newGame() = []

let roll (pins : int) (game : Game) : Game =
    match game with
    | [] -> [ pins ] :: game
    | g when List.length g = 10 -> (pins :: List.head g) :: List.tail g
    | first :: rest ->
        match first with
        | [ 10 ]
        | [ _; _ ] -> [ pins ] :: game
        | [ x ] -> [ pins; x ] :: rest
        | _ -> failwith "wrong frames"

let checkLastFrame frame =
    match frame with
    | [ 10; 10; x ] -> x <= 10
    | [ 10; x; y ] -> x + y <= 10
    | [ x; y; z ] -> x + y = 10 && z <= 10
    | [ x; y ] -> x + y < 10
    | _ -> false

let scoreFrame frame (rest : int list) =
    match frame with
    | [ 10 ] -> 10 + (rest.[0] + rest.[1])
    | [ x; y ] when x + y = 10 -> 10 + rest.[0]
    | _ -> frame |> List.sum

let rec scoreFrames (frames : int list list) (last : int list) =
    match frames with
    | [] -> last |> List.sum
    | first :: rest ->
        scoreFrame first ((rest |> List.concat) @ last) + scoreFrames rest last

let score (game : Game) : int option =
    if List.length game <> 10 then None
    else
        let realGame =
            game
            |> List.map List.rev
            |> List.rev

        let firstFrames = realGame |> List.take 9
        let lastFrame = realGame |> List.item 9
        if checkLastFrame lastFrame
           && List.forall (fun f -> List.sum f <= 10) firstFrames then
            scoreFrames firstFrames lastFrame |> Some
        else None
