﻿module HighScores

let scores = id
let latest = List.last
let highest = List.max

let top : int list -> int list =
    List.sort
    >> List.rev
    >> List.mapi (fun i v ->
           if i < 3 then [ v ]
           else [])
    >> List.concat

let report (values : int list) : string =
    let l, h = latest values, highest values
    (sprintf "Your latest score was %d. " l)
    + (if l = h then "That's your personal best!"
       else sprintf "That's %d short of your personal best!" (h - l))
