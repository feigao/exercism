﻿module Minesweeper

open System

let neighbor8 (r, c) =
    [ for dr in [ -1; 0; 1 ] do
          for dc in [ -1; 0; 1 ] do
              yield (r + dr, c + dc) ]
    |> List.filter ((<>) (r, c))

let annotate (input : string list) =
    match input with
    | [] -> []
    | [ "" ] -> [ "" ]
    | _ ->
        let mines =
            input
            |> List.mapi (fun r row ->
                   row
                   |> Seq.toList
                   |> List.mapi (fun c x -> ((r, c), x)))
            |> List.concat
            |> Map.ofList
        mines
        |> Map.map (fun p x ->
               match x with
               | ' ' ->
                   p
                   |> neighbor8
                   |> List.sumBy (fun p ->
                          match Map.tryFind p mines with
                          | Some '*' -> 1
                          | _ -> 0)
                   |> (fun x ->
                   if x = 0 then " "
                   else string x)
               | _ -> "*")
        |> Map.toList
        |> List.sort
        |> List.groupBy (fst >> fst)
        |> List.map (snd
                     >> List.map snd
                     >> String.Concat)
