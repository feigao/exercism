﻿module FoodChain

let headers =
    [ []
      [ "I know an old lady who swallowed a fly." ]

      [ "I know an old lady who swallowed a spider.";
        "It wriggled and jiggled and tickled inside her." ]

      [ "I know an old lady who swallowed a bird.";
        "How absurd to swallow a bird!" ]

      [ "I know an old lady who swallowed a cat.";
        "Imagine that, to swallow a cat!" ]

      [ "I know an old lady who swallowed a dog.";
        "What a hog, to swallow a dog!" ]

      [ "I know an old lady who swallowed a goat.";
        "Just opened her throat and swallowed a goat!" ]

      [ "I know an old lady who swallowed a cow.";
        "I don't know how she swallowed a cow!" ] ]

let tails =
    [ "She swallowed the cow to catch the goat.";
      "She swallowed the goat to catch the dog.";
      "She swallowed the dog to catch the cat.";
      "She swallowed the cat to catch the bird.";
      "She swallowed the bird to catch the spider that wriggled and jiggled and tickled inside her.";
      "She swallowed the spider to catch the fly.";
      "I don't know why she swallowed the fly. Perhaps she'll die." ]

let paragraph index =
    match index with
    | 8 ->
        [ "I know an old lady who swallowed a horse."; "She's dead, of course!" ]
    | n ->
        headers.[n] @ (tails
                       |> List.rev
                       |> List.take n
                       |> List.rev)

let recite start stop : string list =
    [ start..stop ]
    |> List.map paragraph
    |> List.reduce (fun s1 s2 -> s1 @ [ "" ] @ s2)
