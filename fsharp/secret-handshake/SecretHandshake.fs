﻿module SecretHandshake

let flip f a b = f b a

let handshake =
    [ (1, flip List.append [ "wink" ])
      (2, flip List.append [ "double blink" ])
      (4, flip List.append [ "close your eyes" ])
      (8, flip List.append [ "jump" ])
      (16, List.rev) ]

let commands number =
    handshake
    |> List.fold (fun l (b, f) ->
           if b &&& number <> 0 then f l
           else l) []
