﻿module Dominoes


let rec pick value pairs =
    match pairs with
    | [] -> []
    | (v1, v2) :: rest ->
        let restSol = pick value rest |> List.map (fun (b,xs) -> (b, (v1,v2):: xs))
        if v1 = value then
            (v2, rest) :: restSol
        elif v2 = value then
            (v1, rest) :: restSol
        else
            restSol

let rec aux first last pairs =
    match pairs with
        | [] -> first = last
        | _ ->
            pick last pairs
            |> List.exists (fun (newLast, rest) -> aux first newLast rest)

let canChain (input : (int * int) list) =
    match input with
    | [] -> true
    | (x,y)::rest -> aux x y rest
