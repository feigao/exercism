﻿module CollatzConjecture

let rec steps (number : int) : int option =
    match number with
    | n when n <= 0 -> None
    | 1 -> Some 0
    | n when n % 2 = 0 -> Option.map ((+) 1) (steps (n / 2))
    | n -> Option.map ((+) 1) (steps (n * 3 + 1))
