﻿module VariableLengthQuantity
open System


let rec encodeValue (isLastByte : bool) (input : uint32 ) : uint8 list =
    match isLastByte with
    | true ->
        if input < 128u
            then [uint8 input]
            else encodeValue false (input >>> 7) @ [(input &&& 0x7fu |> uint8)]
    | false ->
        if input = 0u
            then []
            else encodeValue false (input >>> 7) @ [(input &&& 0x7fu |> uint8) ||| 0x80uy]

let encode (input : uint32 list) : uint8 list =
    input
    |> List.collect (encodeValue true)

let isLastByte b = b &&& 0x80uy = 0uy

let rec split (input : uint8 list) =
    let heads = input |> List.takeWhile (isLastByte >> not)
    match input |> List.skip (heads |> List.length) with
    | last::rest -> [heads @ [last]] @ split rest
    | _ -> []

let decodeValue (input : uint8 list) =
    input
    |> List.fold (fun acc v -> acc * 128u + uint32 (v &&& 0x7fuy)) 0u

let decode (input : uint8 list) : uint32 list option =
    match input with
    | [] -> Some []
    | values ->
        if values |> List.last |> isLastByte |> not
            then None
            else input |> split |> List.map decodeValue |> Some
