﻿module RobotName

open System

type Robot = Robot of string

let mutable allNames = Set.empty
let randGen = System.Random()
let genUpperChar() = char (int ('A') + randGen.Next 26)
let genDigit() = char (int ('0') + randGen.Next 10)

let genName() =
    String([| genUpperChar()
              genUpperChar()
              genDigit()
              genDigit()
              genDigit() |])

let rec mkRobot() =
    let newName = genName()
    match Set.contains newName allNames with
    | true -> mkRobot()
    | false ->
        allNames <- allNames.Add(newName)
        Robot newName

let name (Robot name) = name

let reset (Robot name) =
    match Set.contains name allNames with
    | true ->
        allNames <- Set.remove name allNames
        mkRobot()
    | false -> mkRobot()
