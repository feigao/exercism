﻿module ProteinTranslation

let mapOfKeysAndValue (ksv : ('a list * 'b) list) : Map<'a, 'b> =
    ksv
    |> List.collect (fun (ks, v) -> List.map (fun k -> (k, v)) ks)
    |> Map.ofList

let codonProtein =
    [ ([ "AUG" ], "Methionine")
      ([ "UUU"; "UUC" ], "Phenylalanine")
      ([ "UUA"; "UUG" ], "Leucine")
      ([ "UCU"; "UCC"; "UCA"; "UCG" ], "Serine")
      ([ "UAU"; "UAC" ], "Tyrosine")
      ([ "UGU"; "UGC" ], "Cysteine")
      ([ "UGG" ], "Tryptophan")
      ([ "UAA"; "UAG"; "UGA" ], "STOP") ]
    |> mapOfKeysAndValue

let flip f a b = f b a

let proteins (rna : string) : string list =
    rna
    |> Seq.toList
    |> List.chunkBySize 3
    |> List.map (List.map string >> String.concat "")
    |> List.collect (flip Map.tryFind codonProtein >> Option.toList)
    |> List.takeWhile ((<>) "STOP")
