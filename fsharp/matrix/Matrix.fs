﻿module Matrix

let parse (input : string) : int list list =
    input.Split([| '\n' |])
    |> Seq.map (fun row ->
           row.Split([| ' ' |])
           |> List.ofSeq
           |> List.map int)
    |> Seq.toList

let row index matrix =
    matrix
    |> parse
    |> List.item index


let column index matrix =
    matrix
    |> parse
    |> List.transpose
    |> List.item index
