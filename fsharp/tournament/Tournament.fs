﻿module Tournament

open System

type GameResult =
    | Win
    | Draw
    | Loss

type Team =
    { Name : string
      MatchPlayed : int
      Win : int
      Draw : int
      Loss : int }

let point t = 3 * t.Win + 1 * t.Draw

let gameResult =
    function
    | "win" -> Win
    | "draw" -> Draw
    | "loss" -> Loss
    | _ -> failwith "unknown result"

let reverseResult =
    function
    | Win -> Loss
    | Loss -> Win
    | Draw -> Draw

let win t =
    { t with MatchPlayed = t.MatchPlayed + 1
             Win = t.Win + 1 }

let draw t =
    { t with MatchPlayed = t.MatchPlayed + 1
             Draw = t.Draw + 1 }

let loss t =
    { t with MatchPlayed = t.MatchPlayed + 1
             Loss = t.Loss + 1 }

let updateTeam t r =
    match r with
    | Win -> win t
    | Draw -> draw t
    | Loss -> loss t

let mkTeam name =
    { Name = name
      MatchPlayed = 0
      Win = 0
      Draw = 0
      Loss = 0 }

let header = "Team                           | MP |  W |  D |  L |  P"

let tally (rows : string list) =
    header
    :: (rows
        |> List.fold (fun acc row ->
               match row.Split([| ';' |]) with
               | [| n1; n2; rs |] ->
                   let r1 = gameResult rs
                   let r2 = reverseResult r1

                   let acc1 =
                       match Map.tryFind n1 acc with
                       | Some t1 -> Map.add n1 (updateTeam t1 r1) acc
                       | None -> Map.add n1 (updateTeam (mkTeam n1) r1) acc

                   let acc2 =
                       match Map.tryFind n2 acc with
                       | Some t2 -> Map.add n2 (updateTeam t2 r2) acc1
                       | None -> Map.add n2 (updateTeam (mkTeam n2) r2) acc1

                   acc2
               | _ -> failwith (sprintf "row format error: %s" row)) Map.empty
        |> Map.toList
        |> List.map snd
        |> List.sortBy (fun t -> (-(point t), t.Name))
        |> List.map
               (fun t ->
               sprintf "%-31s| %2d | %2d | %2d | %2d | %2d" t.Name t.MatchPlayed
                   t.Win t.Draw t.Loss (point t)))
