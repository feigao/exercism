﻿module PascalsTriangle

let rec rows numberOfRows : int list list =
    match numberOfRows with
    | 0 -> []
    | 1 -> [[1]]
    | n ->
        let rest = rows (numberOfRows - 1)
        let last = List.last rest
        let newRow = last |> List.pairwise |> List.map (fun (x,y) -> x+y)
        rest @ [[1] @ newRow @ [1]]