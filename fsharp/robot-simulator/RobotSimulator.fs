﻿module RobotSimulator

type Direction =
    | North
    | East
    | South
    | West

type Position = int * int

type Robot =
    { direction : Direction
      position : Position }

let create direction position =
    { direction = direction
      position = position }

let advance ({ direction = d; position = (r, c) } as robot) =
    match d with
    | North -> { robot with position = (r, c + 1) }
    | South -> { robot with position = (r, c - 1) }
    | West -> { robot with position = (r - 1, c) }
    | East -> { robot with position = (r + 1, c) }

let turnRight ({ direction = d; position = p } as r) =
    match d with
    | North -> { r with direction = East }
    | East -> { r with direction = South }
    | South -> { r with direction = West }
    | West -> { r with direction = North }

let turnLeft ({ direction = d; position = p } as r) =
    match d with
    | South -> { r with direction = East }
    | West -> { r with direction = South }
    | North -> { r with direction = West }
    | East -> { r with direction = North }

let step ch robot =
    match ch with
    | 'A' -> advance robot
    | 'R' -> turnRight robot
    | 'L' -> turnLeft robot
    | _ -> robot

let move instructions robot =
    Seq.fold (fun r ch -> step ch r) robot instructions
