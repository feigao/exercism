﻿module DndCharacter

open System

let modifier x =
    match x - 10 with
    | value when value % 2 = 0 -> value / 2
    | value -> (value - 1) / 2

let ability() =
    let rgen = Random()
    let rolls = [
        for _ in [1..4] do
        yield ((rgen.Next 6) + 1)
    ]
    (List.sum rolls) - (List.min rolls)

type DndCharacter() =
    let strength = ability ()
    let dex = ability ()
    let con = ability ()
    let intl  = ability ()
    let wisd = ability ()
    let chari = ability ()
    member __.Strength with get() = strength
    member __.Dexterity with get() = dex
    member __.Constitution with get() = con
    member __.Intelligence with get() = intl
    member __.Wisdom with get() = wisd
    member __.Charisma with get() = chari
    member __.Hitpoints with get() = 10 + (modifier con)
