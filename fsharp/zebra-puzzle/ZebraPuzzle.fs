﻿module ZebraPuzzle
open System

type Color =
    | Yellow
    | Red
    | Green
    | Ivory
    | Blue

type Drink =
    | Milk
    | Coffee
    | Tea
    | OrangeJuice
    | Water

type National =
    | Englishman
    | Spaniard
    | Ukrainian
    | Norwegian
    | Japanese

type Pet =
    | Dog
    | Snail
    | Fox
    | Horse
    | Zebra

type Cigar =
    | Kool
    | Chesterfields
    | LuckyStrike
    | Parliaments
    | OldGold

type House =
    { Id : int
      Drink : Drink option
      Nation : National option
      Pet : Pet option
      Color : Color option
      Cigar : Cigar option }

let getId h = h.Id
let getDrink h = h.Drink
let getNation h = h.Nation
let getPet h = h.Pet
let getColor h = h.Color
let getCigar h = h.Cigar

type Solution = House list

let rec distribute e =
    function
    | [] -> [ [ e ] ]
    | x :: xs' as xs ->
        (e :: xs) :: [ for xs in distribute e xs' -> x :: xs ]

let rec permutation =
    function
    | [] -> [ [] ]
    | e :: xs -> List.collect (distribute e) (permutation xs)

let optionCheck getter expected house =
    match getter house with
    | None -> true
    | Some value -> value = expected

let optionCompare p1 v1 p2 v2 h =
    match (p1 h, p2 h) with
    | (None, _) -> true
    | (_, None) -> true
    | (Some v1', Some v2') -> (v1 = v1') && (v2 = v2')

let nextTo h1 h2 =
    match (h1, h2) with
    | None, _ -> true
    | _, None -> true
    | Some h1', Some h2' -> abs (getId h1' - getId h2') = 1

let rules : (Solution -> bool) list =
    [ // The Englishman lives in the red house.
      List.exists (optionCompare getNation Englishman getColor Red)
      // The Spaniard owns the dog.
      List.exists (optionCompare getNation Spaniard getPet Dog)
      // Coffee is drunk in the green house.
      List.exists (optionCompare getDrink Coffee getColor Green)
      // The Ukrainian drinks tea.
      List.exists (optionCompare getNation Ukrainian getDrink Tea)
      // The green house is immediately to the right of the ivory house.
      fun hs ->
          let h1 =
              (List.tryPick (fun h ->
                   if getColor h = Some Green then Some h
                   else None) hs)

          let h2 =
              (List.tryPick (fun h ->
                   if getColor h = Some Ivory then Some h
                   else None) hs)

          match (h1, h2) with
          | (Some h1', Some h2') -> h1'.Id = h2'.Id + 1
          | _ -> true
      // The Old Gold smoker owns snails.
      List.exists (optionCompare getCigar OldGold getPet Snail)
      // Kools are smoked in the yellow house.
      List.exists (optionCompare getCigar Kool getColor Yellow)
      // Milk is drunk in the middle house.
      fun hs ->
          List.tryFind (getId >> (=) 2) hs
          |> Option.map (optionCheck getDrink Milk)
          |> Option.defaultValue true
      // The Norwegian lives in the first house.
      fun hs ->
          List.tryFind (getId >> (=) 0) hs
          |> Option.map (optionCheck getNation Norwegian)
          |> Option.defaultValue true
      // The man who smokes Chesterfields lives in the house next to the man with the fox.
      fun houses ->
          nextTo (List.tryPick (fun h ->
                      if getCigar h = Some Chesterfields then Some h
                      else None) houses) (List.tryPick (fun h ->
                                              if getPet h = Some Fox then Some h
                                              else None) houses)
      // Kools are smoked in the house next to the house where the horse is kept.
      fun hs ->
          nextTo (List.tryPick (fun h ->
                      if getCigar h = Some Kool then Some h
                      else None) hs) (List.tryPick (fun h ->
                                          if getPet h = Some Horse then Some h
                                          else None) hs)
      // The Lucky Strike smoker drinks orange juice.
      List.exists (optionCompare getCigar LuckyStrike getDrink OrangeJuice)
      // The Japanese smokes Parliaments.
      List.exists (optionCompare getCigar Parliaments getNation Japanese)
      // The Norwegian lives next to the blue house.
      fun hs ->
          nextTo (List.tryPick (fun h ->
                      if getNation h = Some Norwegian then Some h
                      else None) hs) (List.tryPick (fun h ->
                                          if getColor h = Some Blue then Some h
                                          else None) hs) ]

let applyAllRules hss = List.fold (fun hss r -> List.filter r hss) hss rules

let guessColor (listOfHs : Solution list) : Solution list =
    [ for cands in permutation [ Red; Blue; Ivory; Yellow; Green ] do
          for hs in listOfHs do
              yield List.map2 (fun h c -> { h with Color = Some c }) hs cands ]

let guessPet (listOfHs : Solution list) : Solution list =
    [ for cands in permutation [ Dog; Fox; Horse; Snail; Zebra ] do
          for hs in listOfHs do
              yield List.map2 (fun h c -> { h with Pet = Some c }) hs cands ]

let guessCigar (listOfHs : Solution list) : Solution list =
    [ for cands in permutation
                       [ Chesterfields; Kool; LuckyStrike; OldGold; Parliaments ] do
          for hs in listOfHs do
              yield List.map2 (fun h c -> { h with Cigar = Some c }) hs cands ]

let guessNation (listOfHs : Solution list) : Solution list =
    [ for cands in permutation
                       [ Englishman; Norwegian; Japanese; Spaniard; Ukrainian ] do
          for hs in listOfHs do
              yield List.map2 (fun h c -> { h with Nation = Some c }) hs cands ]

let guessDrink (listOfHs : Solution list) : Solution list =
    [ for cands in permutation [ Milk; Water; OrangeJuice; Tea; Coffee ] do
          for hs in listOfHs do
              yield List.map2 (fun h c -> { h with Drink = Some c }) hs cands ]

let newHouse id =
    { Id = id
      Color = None
      Pet = None
      Drink = None
      Cigar = None
      Nation = None }

let guess0 =
    [ [ for id in [ 0..4 ] do
            yield { Id = id
                    Color = None
                    Drink = None
                    Pet = None
                    Cigar = None
                    Nation = None } ] ]

let answer =
    guess0
    |> guessNation
    |> applyAllRules
    |> guessColor
    |> applyAllRules
    |> guessPet
    |> applyAllRules
    |> guessCigar
    |> applyAllRules
    |> guessDrink
    |> applyAllRules
    |> List.head

let testRules hss =
    rules |> List.map (fun r -> List.filter r hss |> List.length)

/// test
let drinksWater =
    List.tryFind (optionCheck getDrink Water) answer
    |> Option.bind getNation
    |> Option.get // Norwegian

let ownsZebra =
    List.tryFind (optionCheck getPet Zebra) answer
    |> Option.bind getNation // Japanese
    |> Option.get
