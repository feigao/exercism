module OcrNumbers

open System

let flip f a b = f b a

let assemble (input : char list list) =
    input
    |> List.map String.Concat
    |> String.Concat

let numbers =
    [ " _     _  _     _  _  _  _  _ "; "| |  | _| _||_||_ |_   ||_||_|";
      "|_|  ||_  _|  | _||_|  ||_| _|"; "                              " ]
    |> List.map (List.ofSeq >> List.chunkBySize 3)
    |> List.transpose
    |> List.map assemble
    |> flip List.zip [ 0..9 ]
    |> Map.ofList

let tryFind (input : char list list) =
    if List.length input = 4 && List.forall (List.length >> (=) 3) input then
        match Map.tryFind (assemble input) numbers with
        | None -> Some "?"
        | Some n ->
            n
            |> string
            |> Some
    else None

let convertRow (row : string list) =
    row
    |> List.map (List.ofSeq >> List.chunkBySize 3)
    |> List.transpose
    |> List.map tryFind
    |> List.fold (fun acc os ->
           match (acc, os) with
           | Some ac, Some s -> Some(ac + s)
           | _ -> None) (Some "")

let convert (input : string list) =
    input
    |> List.chunkBySize 4
    |> List.map convertRow
    |> List.reduce (Option.map2 (fun s1 s2 -> s1 + "," + s2))
