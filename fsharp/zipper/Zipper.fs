﻿module Zipper

type Tree<'a> =
    { Value : 'a
      Left : Tree<'a> option
      Right : Tree<'a> option }

let tree value left right =
    { Value = value
      Left = left
      Right = right }

type Direction =
    | Left
    | Right

type Constructor<'a> = 'a * Tree<'a> option * Direction

type Zipper<'a> =
    { Focus : Tree<'a>
      Constructs : Constructor<'a> list }

let restore ((value, subTree, toBeFilled) : Constructor<'a>) (t : Tree<'a>) : Tree<'a> =
    match toBeFilled with
    | Left -> tree value (Some t) subTree
    | Right -> tree value subTree (Some t)

let fromTree (tree : Tree<'a>) : Zipper<'a> =
    { Focus = tree
      Constructs = [] }

let rec toTree (zipper : Zipper<'a>) : Tree<'a> =
    match zipper.Constructs with
    | [] -> zipper.Focus
    | f :: fs ->
        toTree { Focus = restore f zipper.Focus
                 Constructs = fs }

let value (zipper : Zipper<'a>) : 'a = zipper.Focus.Value

let left (zipper : Zipper<'a>) : Zipper<'a> option =
    zipper.Focus.Left
    |> Option.bind
           (fun newFocus ->
           Some
               { Focus = newFocus
                 Constructs =
                     (zipper.Focus.Value, zipper.Focus.Right, Left)
                     :: zipper.Constructs })

let right (zipper : Zipper<'a>) : Zipper<'a> option =
    zipper.Focus.Right
    |> Option.bind
           (fun newFocus ->
           Some
               { Focus = newFocus
                 Constructs =
                     (zipper.Focus.Value, zipper.Focus.Left, Right)
                     :: zipper.Constructs })

let up (zipper : Zipper<'a>) : Zipper<'a> option =
    match zipper.Constructs with
    | [] -> None
    | f :: fs ->
        Some { Focus = restore f zipper.Focus
               Constructs = fs }

let setValue (value : 'a) (zipper : Zipper<'a>) : Zipper<'a> =
    { zipper with Focus = { zipper.Focus with Value = value } }
let setLeft (left : Tree<'a> option) (zipper : Zipper<'a>) =
    { zipper with Focus = tree zipper.Focus.Value left zipper.Focus.Right }
let setRight (right : Tree<'a> option) (zipper : Zipper<'a>) =
    { zipper with Focus = tree zipper.Focus.Value zipper.Focus.Left right }
