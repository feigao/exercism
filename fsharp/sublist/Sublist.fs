﻿module Sublist

type SublistType =
    | Equal
    | Sublist
    | Superlist
    | Unequal

let sublist (xs : 'a list) (ys : 'a list) =
    if List.length xs = List.length ys && xs = ys then Equal
    elif List.isEmpty xs
         || List.length xs < List.length ys
            && List.exists ((=) xs) (List.windowed (List.length xs) ys) then
        Sublist
    elif List.isEmpty ys
         || List.length ys < List.length xs
            && List.exists ((=) ys) (List.windowed (List.length ys) xs) then
        Superlist
    else Unequal
