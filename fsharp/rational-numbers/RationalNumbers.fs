﻿module RationalNumbers

type Rational =
    { Nomerator : int
      Denominator : int }

let rec gcd a b =
    if a = 0 then b
    else gcd (b % a) a

let reduce r =
    let n, d = r.Nomerator, r.Denominator
    let c = gcd n d
    let n', d' = n / c, d / c
    if d' > 0 then
        { Nomerator = n'
          Denominator = d' }
    else
        { Nomerator = -n'
          Denominator = -d' }

let create numerator denominator =
    { Nomerator = numerator
      Denominator = denominator }
    |> reduce

let add r1 r2 =
    create (r1.Nomerator * r2.Denominator + r1.Denominator * r2.Nomerator)
        (r1.Denominator * r2.Denominator) |> reduce
let sub r1 r2 =
    create (r1.Nomerator * r2.Denominator - r1.Denominator * r2.Nomerator)
        (r1.Denominator * r2.Denominator) |> reduce
let mul r1 r2 =
    create (r1.Nomerator * r2.Nomerator) (r1.Denominator * r2.Denominator)
    |> reduce
let div r1 r2 =
    create (r1.Nomerator * r2.Denominator) (r1.Denominator * r2.Nomerator)
    |> reduce
let abs r =
    create (r.Nomerator * (sign r.Nomerator))
        (r.Denominator * (sign r.Denominator)) |> reduce
let exprational n r = create (pown r.Nomerator n) (pown r.Denominator n)
let expreal r n = float n ** (float r.Nomerator / float r.Denominator)
