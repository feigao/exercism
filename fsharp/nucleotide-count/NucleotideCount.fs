﻿module NucleotideCount

let update om c =
    match om
          |> Option.bind (Map.tryFind c)
          |> Option.map ((+) 1) with
    | None -> None
    | Some v -> om |> Option.map (Map.add c v)

let nucleotideCounts (strand : string) : Option<Map<char, int>> =
    let empty =
        [ ('A', 0)
          ('C', 0)
          ('G', 0)
          ('T', 0) ]
        |> Map.ofList
    strand |> Seq.fold update (Some empty)
