﻿module AtbashCipher

open System
open FSharp.Core

let az = [ 'a'..'z' ]
let za = az |> List.rev
let azToza = List.zip az za |> Map.ofList
let zaToaz = List.zip za az |> Map.ofList

let encode (str : string) =
    str
    // |> Array.ofSeq
    |> Seq.filter (Char.IsLetterOrDigit)
    |> Seq.map (fun c ->
           match Map.tryFind (Char.ToLower c) azToza with
           | Some x -> x
           | None -> c)
    |> Seq.chunkBySize 5
    |> Seq.map (String.Concat)
    |> String.concat " "

let decode (str : string) : string =
    str
    |> Seq.filter (Char.IsLetterOrDigit)
    |> Seq.map (fun c ->
           match Map.tryFind (Char.ToLower c) zaToaz with
           | Some c -> c
           | _ -> c)
    |> String.Concat
