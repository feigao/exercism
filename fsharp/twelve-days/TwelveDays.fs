﻿module TwelveDays

open System

let words =
    [ "first"; "second"; "third"; "fourth"; "fifth"; "sixth"; "seventh";
      "eighth"; "ninth"; "tenth"; "eleventh"; "twelfth" ]
let numberWord =
    List.fold2 (fun map idx value -> Map.add idx value map) Map.empty [ 1..12 ]
        words
let lyric =
    [ "twelve Drummers Drumming"; "eleven Pipers Piping"; "ten Lords-a-Leaping";
      "nine Ladies Dancing"; "eight Maids-a-Milking"; "seven Swans-a-Swimming";
      "six Geese-a-Laying"; "five Gold Rings"; "four Calling Birds";
      "three French Hens"; "two Turtle Doves"; "a Partridge in a Pear Tree" ]
let numberLyric =
    List.fold2 (fun map idx value -> Map.add idx value map) Map.empty
        [ 12..-1..1 ] lyric

let prefix day =
    match numberWord.TryFind day with
    | Some word ->
        sprintf "On the %s day of Christmas my true love gave to me" word
    | None -> ""

let suffix day =
    let parts =
        [ day..(-1)..1 ]
        |> List.map numberLyric.TryFind
        |> List.filter Option.isSome
        |> List.map Option.get
    match parts.Length with
    | 1 -> parts.Head
    | len when len >= 2 ->
        String.concat ", " (parts.GetSlice(None, Some(len - 2)))
        + ", and " + parts.[len-1]
    | _ -> ""

let sentence day =
    String.concat ": " [ prefix day
                         suffix day ]
    + "."
let recite start stop = [ start..stop ] |> List.map sentence
