﻿module SumOfMultiples

let sum (numbers : int list) (upperBound : int) : int =
    numbers
    |> List.filter ((<) 0)
    |> List.collect (fun n -> [ 0..n..(upperBound - 1) ])
    |> List.distinct
    |> List.sum
