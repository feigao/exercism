﻿module ParallelLetterFrequency

open System

module Map =
    let updateWithDefault key update defaultValue map =
        match Map.tryFind key map with
        | None -> Map.add key (update defaultValue) map
        | Some v -> Map.add key (update v) map

    let keys table =
        table
        |> Map.toSeq
        |> Seq.map fst

    let merge (merger : 'T option -> 'T option -> 'T option)
        (m1 : Map<'Key, 'T>) (m2 : Map<'Key, 'T>) =
        let keys1 =
            m1
            |> keys
            |> Set

        let keys2 =
            m2
            |> keys
            |> Set

        Set.fold (fun acc key ->
            match merger (Map.tryFind key m1) (Map.tryFind key m2) with
            | None -> acc
            | Some v -> Map.add key v acc) Map.empty (Set.union keys1 keys2)

let mapAdd1 m c' =
    if Char.IsLetter c' then
        let c = Char.ToLower c'
        Map.updateWithDefault c ((+) 1) 0 m
    else m

let worker (input : string) = async { return Seq.fold mapAdd1 Map.empty input }

let optionAdd a b =
    match a, b with
    | Some a, Some b -> a + b |> Some
    | Some a, None -> Some a
    | None, Some b -> Some b
    | _ -> None

let frequency (texts : string list) =
    texts
    |> Seq.map worker
    |> Async.Parallel
    |> Async.RunSynchronously
    |> Seq.fold (Map.merge optionAdd) Map.empty
