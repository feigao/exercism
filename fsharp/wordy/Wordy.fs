module Wordy

open FParsec

let answer question =
    let pContent =
        Primitives.chainl1 (pint32 .>> spaces)
            (choice [ stringReturn "plus" (+) .>> spaces
                      stringReturn "minus" (-) .>> spaces
                      stringReturn "multiplied by" (*) .>> spaces
                      stringReturn "divided by" (/) .>> spaces ])

    let parser =
        pContent
        |> between (pstring "What is " .>> spaces) (pstring "?" .>> eof)
    match run parser question with
    | Success(result, _, _) -> Some result
    | _ -> None
