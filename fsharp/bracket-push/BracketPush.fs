﻿module BracketPush

let isOpen c =
    match c with
    | '('
    | '['
    | '{' -> true
    | _ -> false

let isClose c =
    match c with
    | ')'
    | ']'
    | '}' -> true
    | _ -> false

let matchParens left right =
    match left, right with
    | '(', ')' -> true
    | '[', ']' -> true
    | '{', '}' -> true
    | _ -> false

let isPaired (input : string) =
    let stack, acc =
        List.fold (fun (stack, acc) c ->
            if not acc then (stack, acc)
            elif isOpen c then (c :: stack, acc)
            elif isClose c then
                match stack with
                | [] -> ([], false)
                | o :: rest ->
                    if matchParens o c then (rest, acc)
                    else ([], false)
            else (stack, acc)) ([], true) (input |> Seq.toList)
    acc && List.isEmpty stack
