﻿module Hangman

open System

type Progress =
    | Busy of int
    | Win
    | Lose
    | Ready

type Model =
    { answer : char list
      guesses : char list
      progress : Progress
      maskedWord : string }

type Msg =
    | Guess of char
    | Start
    | Reduce

type Game(initState : Model, updateFunc : Msg -> Model -> Model) =
    let mutable state = initState
    let mutable handlers = []
    let update = updateFunc

    let loop msg =
        let newState = update msg state
        for handler in handlers do
            handler newState
        state <- newState

    member __.Guess ch = loop (Guess ch)
    member __.Start() = loop Start
    member this.Observar = this
    member __.Add h = handlers <- h :: handlers

let rec update (msg : Msg) (model : Model) : Model =
    match msg with
    | Start -> { model with progress = Busy 9 }
    | Guess g ->
        match model.progress with
        | Busy n ->
            if List.contains g model.answer
               && (List.contains g model.guesses |> not) then
                update Reduce { model with guesses = g :: model.guesses
                                           progress = Busy n }
            else
                update Reduce { model with guesses = g :: model.guesses
                                           progress = Busy(n - 1) }
        | _ -> model
    | Reduce ->
        match model.progress with
        | Busy 0 -> { model with progress = Lose }
        | Busy n ->
            { model with maskedWord =
                             model.answer
                             |> List.map (fun c ->
                                    if List.contains c model.guesses then c
                                    else '_')
                             |> String.Concat
                         progress =
                             if model.answer
                                |> List.forall
                                       (fun c -> List.contains c model.guesses) then
                                 Win
                             else Busy n }
        | _ -> model

let createGame (answer : string) =
    let initState =
        { answer = answer |> Seq.toList
          progress = Ready
          guesses = []
          maskedWord = answer |> String.map (fun _ -> '_') }
    Game(initState, update)

let statesObservable (game : Game) = game.Observar
let startGame (game : Game) = game.Start()
let makeGuess (guess : char) (game : Game) = game.Guess guess
