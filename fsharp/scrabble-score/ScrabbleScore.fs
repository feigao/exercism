﻿module ScrabbleScore

let rawLetterValue =
    [ ([ 'A'; 'E'; 'I'; 'O'; 'U'; 'L'; 'N'; 'R'; 'S'; 'T' ], 1)
      ([ 'D'; 'G' ], 2)
      ([ 'B'; 'C'; 'M'; 'P' ], 3)
      ([ 'F'; 'H'; 'V'; 'W'; 'Y' ], 4)
      ([ 'K' ], 5)
      ([ 'J'; 'X' ], 8)
      ([ 'Q'; 'Z' ], 10) ]

let charScore : Map<char, int> =
    List.fold (fun m (cs, v) -> List.fold (fun m' c -> Map.add c v m') m cs)
        Map.empty rawLetterValue
let flip f a b = f b a

let score : string -> int =
    Seq.toList
    >> List.sumBy (System.Char.ToUpper
                   >> flip Map.tryFind charScore
                   >> Option.toList
                   >> List.sum)
