﻿module PhoneNumber

open System

let getNumbers input  =
    let input' = input |> List.filter ((<>) '-')
    if List.exists Char.IsLetter input' then
        Error "alphanumerics not permitted"
    elif List.exists (fun ch -> List.exists ((=) ch) ['!'; '@'; ':']) input'
        then Error "punctuations not permitted"
    else
        input'
        |> List.filter Char.IsDigit
        |> Ok

let checkLength input =
    match List.length input with
    | 10 -> Ok input
    | 11 ->
        match input with
        | '1'::rest -> Ok rest
        | _ -> Error "11 digits must start with 1"
    | l when l > 11 -> Error "more than 11 digits"
    | _ -> Error "incorrect number of digits"

let checkAreaCode (input: List<Char>) =
    if input.[0] = '0' then
        Error "area code cannot start with zero"
    elif input.[0] = '1' then
        Error "area code cannot start with one"
    else Ok input

let checkExchangeCode (input: List<Char>) =
    if input.[3] = '0' then
        Error "exchange code cannot start with zero"
    elif input.[3] = '1' then
        Error "exchange code cannot start with one"
    else Ok input

let constructNumber input =
    match (input |> Array.ofList |> String |> UInt64.TryParse) with
    | (true, value) -> Ok value
    | _ -> Error "not a number"

let clean (input : string): Result<uint64, string> =
    input
    |> Seq.toList
    |> getNumbers
    |> Result.bind checkLength
    |> Result.bind checkAreaCode
    |> Result.bind checkExchangeCode
    |> Result.bind constructNumber
