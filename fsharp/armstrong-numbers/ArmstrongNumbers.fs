﻿module ArmstrongNumbers

let rec intPow n x  =
    match n with
    | 0 -> 1
    | 1 -> x
    | n -> x * intPow (n-1) x

let convert (number : int) : int =
    let str = string number
    let exp = String.length str
    let nums = str |> Seq.toList |> List.map (string >> int)
    nums |> List.sumBy (intPow exp)


let isArmstrongNumber (number: int): bool =
    number = convert number 