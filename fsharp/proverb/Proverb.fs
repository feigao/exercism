﻿module Proverb

let flip f a b = f b a

let recite (input : string list) : string list =
    input
    |> List.pairwise
    |> List.map
           (fun (s1, s2) -> sprintf "For want of a %s the %s was lost." s1 s2)
    |> flip List.append (input
                         |> List.tryHead
                         |> Option.map (sprintf "And all for the want of a %s.")
                         |> Option.toList)
