﻿module DotDsl

type Property = string * string

type Node =
    { Name : string
      Properties : Property list }

and Edge =
    { Left : string
      Right : string
      Properties : Property list }

and Attr =
    { Key : string
      Value : string }

and GraphElement =
    | Node of Node
    | Edge of Edge
    | Attr of Attr

and Graph =
    { Nodes : Node list
      Edges : Edge list
      Attrs : Attr list }

let unNode =
    function
    | Node n -> Some n
    | _ -> None

let unEdge =
    function
    | Edge e -> Some e
    | _ -> None

let unAttr =
    function
    | Attr a -> Some a
    | _ -> None

let attr key value =
    Attr { Key = key
           Value = value }

let node key attrs =
    Node { Name = key
           Properties = attrs }

let edge left right attrs =
    Edge { Left = left
           Right = right
           Properties = attrs }

let graph children =
    { Nodes = List.collect (unNode >> Option.toList) children
      Edges = List.collect (unEdge >> Option.toList) children
      Attrs = List.collect (unAttr >> Option.toList) children }

let attrs ({ Attrs = list } : Graph) =
    list
    |> List.map Attr
    |> List.sort

let nodes ({ Nodes = list } : Graph) =
    list
    |> List.map Node
    |> List.sort

let edges ({ Edges = list } : Graph) =
    list
    |> List.map Edge
    |> List.sort
