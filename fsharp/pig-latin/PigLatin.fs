﻿module PigLatin

open System

let isVowel ch =
    match ch with
    | 'a'
    | 'e'
    | 'i'
    | 'o'
    | 'u' -> true
    | _ -> false

let isConsonant = isVowel >> not
let ay = [ 'a'; 'y' ]

let checkRule1 input =
    match input with
    // rule 1
    | (c :: _) when isVowel c -> true
    | ('x' :: 'r' :: _)
    | ('y' :: 't' :: _) -> true
    | _ -> false

let runRule1 input = List.append input ay

let checkRule2 input =
    input
    |> List.head
    |> isConsonant

let splitBy p l = List.takeWhile p l, List.skipWhile p l

let runRule2 input =
    let cons, rest = splitBy isConsonant input
    rest @ cons @ ay

let checkRule3 input =
    let cons, rest = splitBy isConsonant input
    List.tryLast cons = Some 'q' && List.tryHead rest = Some 'u'

let runRule3 input =
    let cons, rest = splitBy isConsonant input
    List.concat [ List.tail rest
                  cons
                  [ 'u' ]
                  ay ]

let checkRule4 input =
    let cons, _ = splitBy isConsonant input
    input.[0] <> 'y' && List.contains 'y' cons
    || (List.length input = 2 && input.[1] = 'y')

let runRule4 input =
    let head, ys = splitBy ((<>) 'y') input
    ys @ head @ ay

let translateList (input : Char list) : Char list =
    if List.forall Char.IsWhiteSpace input then input
    elif checkRule1 input then runRule1 input
    elif checkRule4 input then runRule4 input
    elif checkRule3 input then runRule3 input
    elif checkRule2 input then runRule2 input
    else input

let rec words input =
    match splitBy (Char.IsLetter) input with
    | ws, [] -> [ ws ]
    | ws, rest ->
        let ss, rs = splitBy Char.IsWhiteSpace rest
        ws :: ss :: words rs

let translate (input : string) =
    input
    |> Seq.toList
    |> words
    |> List.collect translateList
    |> String.Concat
