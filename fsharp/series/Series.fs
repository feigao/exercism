﻿module Series

let slices (str : string) length =
    if String.length str < length || length <= 0 then None
    else
        str
        |> Seq.windowed length
        |> Seq.map (Array.ofSeq >> System.String)
        |> Seq.toList
        |> Some
