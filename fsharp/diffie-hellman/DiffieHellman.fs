﻿module DiffieHellman

open System.Numerics

let rand = System.Random()

let rec privateKey (primeP : BigInteger) =
    let bytes = primeP.ToByteArray()
    rand.NextBytes(bytes)
    bytes.[bytes.Length - 1] <- bytes.[bytes.Length - 1] &&& (byte) 0x7f
    let key = BigInteger(bytes)
    if key >= 2I && key < primeP then key
    else privateKey primeP

let publicKey primeP primeG privateKey : BigInteger =
    BigInteger.ModPow(primeG, privateKey, primeP)
let secret primeP publicKey privateKey : BigInteger =
    BigInteger.ModPow(publicKey, privateKey, primeP)
