﻿module Etl

open System

let transform : Map<int, char list> -> Map<char, int> =
    Map.fold
        (fun mci s cs ->
        List.fold (fun m c -> Map.add (c |> Char.ToLower) s m) mci cs) Map.empty
