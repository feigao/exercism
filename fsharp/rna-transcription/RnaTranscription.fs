﻿module RnaTranscription

let toRna (dna: string): string =
    dna
    |> String.map (
        function
        | 'G' -> 'C'
        | 'C' -> 'G'
        | 'T' -> 'A'
        | 'A' -> 'U'
        | _ -> ' '
    )
