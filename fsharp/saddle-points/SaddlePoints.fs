module SaddlePoints

open System

type ActionT =
    | Replace
    | Update
    | Ignore

let pickPos (comp : int -> int -> ActionT) : list<int> -> list<int> =
    List.fold (fun (idx, acc) item ->
        match acc with
        | [] -> (idx + 1, [ (item, idx) ])
        | (v, _) :: _ ->
            match comp v item with
            | Update -> (idx + 1, (item, idx) :: acc)
            | Replace -> (idx + 1, [ (item, idx) ])
            | Ignore -> (idx + 1, acc)) (0, [])
    >> snd
    >> List.map snd

let saddlePoints (matrix : int list list) : list<int * int> =
    let rows =
        matrix
        |> List.mapi (fun r row ->
               row
               |> pickPos (fun oldV newV ->
                      if newV > oldV then Replace
                      elif newV = oldV then Update
                      else Ignore)
               |> List.map (fun c -> (r, c)))
        |> List.concat
        |> Set

    let cols =
        matrix
        |> List.transpose
        |> List.mapi (fun c col ->
               col
               |> pickPos (fun oldV newV ->
                      if newV < oldV then Replace
                      elif newV = oldV then Update
                      else Ignore)
               |> List.map (fun r -> (r, c)))
        |> List.concat
        |> Set

    Set.intersect rows cols
    |> Set.toList
    |> List.sort
