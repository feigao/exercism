﻿module CircularBuffer

type Buffer<'a> =
    { Capacity : int
      Front : 'a list
      End : 'a list }

let mkCircularBuffer size =
    { Capacity = size
      Front = []
      End = [] }

let clear buffer =
    { buffer with Front = []
                  End = [] }

let write value buffer =
    if buffer.Front.Length + buffer.End.Length = buffer.Capacity then
        raise <| System.Exception "full"
    else { buffer with Front = value :: buffer.Front }

let rec read buffer =
    match buffer with
    | { End = x :: xs } -> (x, { buffer with End = xs })
    | { Front = []; End = [] } -> raise <| System.Exception "empty"
    | { End = []; Front = xs } ->
        read { buffer with Front = []
                           End = xs |> List.rev }

let forceWrite value buffer =
    try
        buffer |> write value
    with :? System.Exception ->
        buffer
        |> read
        |> snd
        |> write value
