﻿module PalindromeProducts

let mul (x, y) = x * y

let rec digits number =
    if number < 0 then failwith "negative number"
    elif number < 10 then [ number ]
    else (number % 10) :: digits (number / 10)

let isPalindrome number =
    let ds = digits number
    ds = (ds |> List.rev)

let rec search pickFunc nextFunc (xys : List<int * int>) =
    if List.isEmpty xys then None
    else
        let p =
            xys
            |> pickFunc
            |> mul

        let selected, rest = xys |> List.partition (mul >> (=) p)
        if isPalindrome p then Some(p, selected |> List.sort)
        else
            selected
            |> List.collect nextFunc
            |> (@) rest
            |> List.distinct
            |> search pickFunc nextFunc

let largest minFactor maxFactor =
    if minFactor > maxFactor then None
    else
        let pick = List.maxBy mul

        let next (x, y) =
            [ (x - 1, y)
              (x, y - 1) ]
            |> List.filter
                   (fun (x, y) -> minFactor <= x && x <= y && y <= maxFactor)
        [ (maxFactor, maxFactor) ] |> search pick next

let smallest minFactor maxFactor =
    if minFactor > maxFactor then None
    else
        let next (x, y) =
            [ (x + 1, y)
              (x, y + 1) ]
            |> List.filter
                   (fun (x, y) -> minFactor <= x && x <= y && y <= maxFactor)

        let pick = List.minBy mul
        [ (minFactor, minFactor) ] |> search pick next
