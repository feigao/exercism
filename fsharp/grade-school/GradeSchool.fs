﻿module GradeSchool

open System

type School = Map<int, string list>

let empty: School = Map.empty

let add (student: string) (grade: int) (school: School): School =
    match Map.tryFind grade school with
    | None -> Map.add grade [student] school
    | Some names -> Map.add grade (student :: names) school

let roster (school: School): string list =
    school
    |> Map.toList
    |> List.sort
    |> List.collect (snd >> List.sort)

let grade (number: int) (school: School): string list =
    match school |> Map.tryFind number with
    | None -> []
    | Some students -> students |> List.sort
