﻿module Sieve

let primes limit =
    let marks = Map.ofList (List.map (fun v -> (v, true)) [ 2..limit ])
    [ 2..limit ]
    |> List.fold (fun (acc, ps) n ->
           if Map.find n acc then
               ([ n..n..limit ] |> List.fold (fun m k -> Map.add k false m) acc,
                n :: ps)
           else (acc, ps)) (marks, [])
    |> snd
    |> List.rev
