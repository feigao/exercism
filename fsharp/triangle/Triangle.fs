﻿module Triangle

let isTriangle (tri : float list) =
    List.length tri = 3
    && (let [ a; b; c ] = tri
        a > 0.0 && b > 0.0 && c > 0.0 && a + b > c && a + c > b && b + c > a)

let equalF a b = abs (a - b) < 0.001

let equilateral triangle =
    isTriangle triangle && (let [ a; b; c ] = triangle
                            equalF a b && equalF a c && equalF b c)

let isosceles triangle =
    isTriangle triangle && (let [ a; b; c ] = triangle
                            equalF a b || equalF a c || equalF b c)

let scalene triangle =
    isTriangle triangle && (let [ a; b; c ] = triangle
                            equalF a b
                            |> not
                            && equalF a c |> not
                            && equalF b c |> not)
