module LinkedList

type DoublyLinkedListNode<'a> =
    { Value : 'a
      mutable Previous : DoublyLinkedListNode<'a> option
      mutable Next : DoublyLinkedListNode<'a> option }

type DoublyLinkedList<'a>() =
    let mutable length = 0
    let mutable front = None
    let mutable back = None
    member this.Length = length
    member this.Front = front
    member this.Back = back

    member this.PushFront(v : 'a) =
        if length = 0 then
            let node =
                { Value = v
                  Previous = None
                  Next = None }
            front <- Some node
            back <- Some node
        else
            let oldFront = front |> Option.get

            let newFront =
                { Value = v
                  Previous = None
                  Next = Some oldFront }
            oldFront.Previous <- Some newFront
            front <- Some newFront
        length <- length + 1

    member this.PushBack(v : 'a) =
        if length = 0 then
            let node =
                { Value = v
                  Previous = None
                  Next = None }
            front <- Some node
            back <- Some node
        else
            let oldBackNode = back |> Option.get

            let newBackNode =
                { Value = v
                  Previous = Some oldBackNode
                  Next = None }
            oldBackNode.Next <- Some newBackNode
            back <- Some newBackNode
        length <- length + 1

    member this.PopFront() : 'a =
        if length = 0 then failwith "PopFront from empty list"
        else
            let oldFrontNode = front |> Option.get
            if length = 1 then
                front <- None
                back <- None
            else
                let oldNext = oldFrontNode.Next |> Option.get
                oldNext.Previous <- None
                front <- Some oldNext
            length <- length - 1
            oldFrontNode.Value

    member this.PopBack() : 'a =
        if length = 0 then failwith "PopBack from empty list"
        else
            let oldBackNode = back |> Option.get
            if length = 1 then
                front <- None
                back <- None
            else
                let oldPrevious = oldBackNode.Previous |> Option.get
                oldPrevious.Next <- None
                back <- Some oldPrevious
            length <- length - 1
            oldBackNode.Value

let mkLinkedList() = DoublyLinkedList()
let addToEmpty (newValue : 'a) (linkedList : DoublyLinkedList<'a>) =
    linkedList.PushFront(newValue)
let pop (linkedList : DoublyLinkedList<'a>) = linkedList.PopBack()
let shift (linkedList : DoublyLinkedList<'a>) = linkedList.PopFront()
let push (newValue : 'a) (linkedList : DoublyLinkedList<'a>) =
    linkedList.PushBack newValue
let unshift (newValue : 'a) (linkedList : DoublyLinkedList<'a>) =
    linkedList.PushFront newValue
