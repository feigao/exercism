﻿module Yacht

type Category =
    | Ones
    | Twos
    | Threes
    | Fours
    | Fives
    | Sixes
    | FullHouse
    | FourOfAKind
    | LittleStraight
    | BigStraight
    | Choice
    | Yacht

type Die =
    | One
    | Two
    | Three
    | Four
    | Five
    | Six

let valueOfDie =
    function
    | One -> 1
    | Two -> 2
    | Three -> 3
    | Four -> 4
    | Five -> 5
    | Six -> 6

let scoreOfOneKind die dice =
    dice
    |> List.sumBy (fun d ->
           if d = die then valueOfDie d
           else 0)

let pattern dice =
    dice
    |> List.countBy id
    |> List.map snd
    |> List.sort

let (|FullHouseP|_|) (dice : Die list) =
    if (dice
        |> pattern = [ 2; 3 ]) then Some FullHouseP
    else None

let (|FourOfAKindP|_|) (dice : Die list) =
    if (dice
        |> pattern
        |> List.max
        |> fun x -> x >= 4)
    then
        let d =
            if dice.[0] = dice.[1] then dice.[0]
            else dice.[2]
        Some(FourOfAKindP d)
    else None

let (|LittleStraightP|_|) dice =
    if dice
       |> List.sort = [ One; Two; Three; Four; Five ] then Some LittleStraightP
    else None

let (|BigStraightP|_|) dice =
    if dice
       |> List.sort = [ Two; Three; Four; Five; Six ] then Some BigStraightP
    else None

let (|YachtP|_|) dice =
    if dice
       |> pattern = [ 5 ] then Some YachtP
    else None

let score category dice =
    match (category, dice) with
    | (Ones, _) -> scoreOfOneKind One dice
    | (Twos, _) -> scoreOfOneKind Two dice
    | (Threes, _) -> scoreOfOneKind Three dice
    | (Fours, _) -> scoreOfOneKind Four dice
    | (Fives, _) -> scoreOfOneKind Five dice
    | (Sixes, _) -> scoreOfOneKind Six dice
    | (FullHouse, FullHouseP) -> dice |> List.sumBy valueOfDie
    | (FourOfAKind, FourOfAKindP v) -> valueOfDie v * 4
    | (LittleStraight, LittleStraightP) -> 30
    | (BigStraight, BigStraightP) -> 30
    | (Choice, _) -> dice |> List.sumBy valueOfDie
    | (Yacht, YachtP) -> 50
    | _ -> 0
