﻿module ScaleGenerator

open System

let scalesWithSharp =
    "A A# B C C# D D# E F F# G G#".Split([| ' ' |]) |> Array.toList
let scalesWithFlat =
    "A Bb B C Db D Eb E F Gb G Ab".Split([| ' ' |]) |> Array.toList

let sharps =
    let s = "G D A E B F# e b f# c# g# d#"
    s.Split([| ' ' |]) |> Array.toList

let flats =
    let s = "F Bb Eb Ab Db Gb d g c f bb eb"
    s.Split([| ' ' |]) |> Array.toList

let useSharps tonic = List.exists ((=) tonic) sharps
let useFlats tonic = List.exists ((=) tonic) flats

let (|Sharp|_|) tonic =
    if useSharps tonic then Some Sharp
    else None

let (|Flat|_|) tonic =
    if useFlats tonic then Some Flat
    else None

let chromatic tonic =
    let scale =
        match tonic with
        | Sharp -> scalesWithSharp
        | Flat -> scalesWithFlat
        | _ -> scalesWithSharp // "C" or "a"
    scale
    |> List.append scale
    |> List.skip (List.findIndex ((=) (String.mapi (fun i c ->
                                           if i = 0 then Char.ToUpper c
                                           else c) tonic)) scale)
    |> List.take (List.length scale)

let interval (intervals : string) tonic =
    let scale = chromatic tonic
    intervals
    |> Seq.fold (fun (s, i) c ->
           let d =
               match c with
               | 'm' -> 1
               | 'M' -> 2
               | 'A' -> 3
               | _ -> failwith "Unknown interval"
           (scale.Item i :: s, i + d)) ([], 0)
    |> fst
    |> List.rev
