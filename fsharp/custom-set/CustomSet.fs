﻿module CustomSet

type CustomSet<'a> = CustomSet of 'a list

let inline flip f a b = f b a
let toList (CustomSet elems) = elems

let fromList list =
    list
    |> List.distinct
    |> CustomSet

let empty = CustomSet []
let singleton value = CustomSet [ value ]

let isEmpty set =
    set
    |> toList
    |> List.isEmpty

let size set =
    set
    |> toList
    |> List.length

let contains value set =
    set
    |> toList
    |> List.contains value

let insert value set = value :: (set |> toList) |> fromList

let filter p set =
    set
    |> toList
    |> List.filter p
    |> fromList

let union left right =
    left
    |> toList
    |> List.fold (flip insert) right

let intersection left right = left |> filter (flip contains right)
let difference left right = left |> filter (flip contains right >> not)
let isSubsetOf left right = difference left right |> isEmpty
let isDisjointFrom left right = intersection left right |> isEmpty
let isEqualTo left right = size left = size right && isSubsetOf left right
