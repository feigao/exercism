﻿module QueenAttack

let create (position: int * int) =
    let r , c = position
    0 <= r && r < 8 && 0 <= c && c < 8

let canAttack (queen1: int * int) (queen2: int * int) =
    let r1, c1 = queen1
    let r2, c2 = queen2
    r1 = r2 || c1 = c2 || r1 - r2 = c1 - c2 || r1 - r2 = c2 - c1