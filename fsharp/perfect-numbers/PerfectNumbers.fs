﻿module PerfectNumbers

type Classification =
    | Perfect
    | Abundant
    | Deficient

let aliquotSum n =
    [ 1..(n - 1) ]
    |> List.filter (fun d -> n % d = 0)
    |> List.sum

let classify n : Classification option =
    if n <= 0 then None
    else
        let asum = aliquotSum n
        if asum = n then Some Perfect
        elif asum < n then Some Deficient
        else Some Abundant
