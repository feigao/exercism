﻿module NthPrime

let rec primes =
    seq {
        yield! [ 2; 3; 5; 7 ]
        for n in Seq.initInfinite (fun i -> 2 * i + 9) do
            match primes // #nowarn "40"
                  |> Seq.takeWhile (fun x -> x * x <= n)
                  |> Seq.forall (fun d -> n % d <> 0) with
            | true -> yield n
            | _ -> ()
    }

let prime nth : int option = primes |> Seq.tryItem (nth - 1)
