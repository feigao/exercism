﻿module TreeBuilding

type Record =
    { RecordId : int
      ParentId : int }

type Tree =
    | Branch of int * Tree list
    | Leaf of int

let recordId t =
    match t with
    | Branch(id, c) -> id
    | Leaf id -> id

let isBranch t =
    match t with
    | Branch(id, c) -> true
    | Leaf id -> false

let children t =
    match t with
    | Branch(id, c) -> c
    | Leaf id -> []

let checkEmpty = List.isEmpty
let checkInvalidRoot root = root.ParentId <> 0 || root.RecordId <> 0

let checkInvalidParents records =
    records
    |> List.tail
    |> List.forall (fun node -> node.ParentId < node.RecordId)
    |> not

let checkContinuousList records =
    records
    |> List.map (fun node -> node.RecordId)
    <> List.init (List.length records) id

let buildTree records =
    let records' = List.sortBy (fun x -> x.RecordId) records
    if checkEmpty records' then failwith "Empty input"
    if checkInvalidRoot records'.[0] then failwith "Root node is invalid"
    if checkInvalidParents records' then failwith "Invalid parents"
    if checkContinuousList records' then failwith "Non-continuous list"

    let map = 
        records'
        |> List.tail
        |> List.map (fun r -> (r.ParentId, r.RecordId))
        |> List.groupBy fst
        |> List.map (fun (x, y) -> (x, List.map snd y))
        |> Map.ofSeq

    let rec helper key =
        if Map.containsKey key map then
            Branch(key, List.map helper (Map.find key map))
        else Leaf key

    let root = helper 0
    root
