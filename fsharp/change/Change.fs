﻿module Change

let findFewestCoins (coins : int list) (target : int) =
    coins
    |> List.sort
    |> List.fold (fun acc c ->
           [ 0..(target - c) ]
           |> List.fold (fun map x ->
                  match (Map.tryFind x map, Map.tryFind (x + c) map) with
                  | Some small, None ->
                      Map.add (x + c) (c :: small |> List.sort) map
                  | Some small, Some large when List.length small + 1 < List.length
                                                                            large ->
                      Map.add (x + c) ((c :: small) |> List.sort) map
                  | _ -> map) acc) ([ (0, []) ] |> Map.ofList)
    |> Map.tryFind target
