﻿module BookStore

open System

let prepare list =
    list
    |> List.filter (fun v -> v > 0)
    |> List.sort
    |> List.rev

let rec cost (books : int list) : float =
    match books with
    | [] -> 0.0
    | [ x ] -> float x * 8.0
    | [ x; y ] -> float y * 8.0 * 2.0 * (1.0 - 0.05) + cost [ x - y ]
    | [ x; y; z ] ->
        List.min [ 3.0 * 8.0 * (1.0 - 0.1) + ([ x - 1
                                                y - 1
                                                z - 1 ]
                                              |> prepare
                                              |> cost)
                   2.0 * 8.0 * (1.0 - 0.05) + ([ x - 1
                                                 y - 1
                                                 z ]
                                               |> prepare
                                               |> cost) ]
    | [ x; y; z; w ] ->
        List.min [ 4.0 * 8.0 * (1.0 - 0.2) + ([ x - 1
                                                y - 1
                                                z - 1
                                                w - 1 ]
                                              |> prepare
                                              |> cost)
                   3.0 * 8.0 * (1.0 - 0.1) + ([ x - 1
                                                y - 1
                                                z - 1
                                                w ]
                                              |> prepare
                                              |> cost)
                   2.0 * 8.0 * (1.0 - 0.05) + ([ x - 1
                                                 y - 1
                                                 z
                                                 w ]
                                               |> prepare
                                               |> cost) ]
    | [ x; y; z; w; v ] ->
        List.min [ 5.0 * 8.0 * (1.0 - 0.25) + ([ x - 1
                                                 y - 1
                                                 z - 1
                                                 w - 1
                                                 v - 1 ]
                                               |> prepare
                                               |> cost)
                   4.0 * 8.0 * (1.0 - 0.2) + ([ x - 1
                                                y - 1
                                                z - 1
                                                w - 1
                                                v ]
                                              |> prepare
                                              |> cost)
                   3.0 * 8.0 * (1.0 - 0.1) + ([ x - 1
                                                y - 1
                                                z - 1
                                                w
                                                v ]
                                              |> prepare
                                              |> cost)
                   2.0 * 8.0 * (1.0 - 0.05) + ([ x - 1
                                                 y - 1
                                                 z
                                                 w
                                                 v ]
                                               |> prepare
                                               |> cost) ]

let total (books : int list) =
    books
    |> List.countBy id
    |> List.map snd
    |> prepare
    |> cost
