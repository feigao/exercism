﻿module AllYourBase

let rebase (digits : int list) (inputBase : int) (outputBase : int) : int list option =
    if inputBase <= 1 || outputBase <= 1
       || (List.forall (fun d -> 0 <= d && d < inputBase) digits) |> not then
        None
    else
        digits
        |> List.fold (fun acc x -> x + inputBase * acc) 0
        |> List.unfold (function
               | 0 -> None
               | n -> (n % outputBase, n / outputBase) |> Some)
        |> List.rev
        |> function
        | [] -> [ 0 ]
        | xs -> xs
        |> Some
