module PythagoreanTriplet

let triple m n d =
    [ m * m - n * n
      2 * m * n
      m * m + n * n ]
    |> List.map ((*) d)
    |> List.sort
    |> function
    | [ x; y; z ] -> (x, y, z)
    | _ -> failwith ""

let sqrtInt n =
    n
    |> float
    |> sqrt
    |> int

let mutable factorCache : Map<int, (int * int) list> = Map.empty

let factors number =
    match Map.tryFind number factorCache with
    | Some v -> v
    | None ->
        let v =
            [ for x in 1..(sqrtInt number) do
                  if number % x = 0 then
                      yield! [ (number / x, x)
                               (x, number / x) ] ]
        factorCache <- Map.add number v factorCache
        v

let primitiveTripletsWithSum sum d =
    if sum % 2 = 1 then []
    else sum / 2 |> factors
    |> List.collect (fun (d1, d2) ->
           let m = min d1 d2
           let n = d1 + d2 - m - m
           if 0 < n && n < m then triple m n d |> List.singleton
           else [])

let tripletsWithSum sum =
    sum
    |> factors
    |> List.collect (fun (d1, d2) -> primitiveTripletsWithSum d1 d2)
    |> List.distinct
    |> List.sort
