﻿module RotationalCipher

let charToIndex c = int c - int 'a'
let indexToChar i = ((i % 26 + 26) % 26) + int 'a' |> char

let shiftChar offset c =
    if System.Char.IsUpper c then
        c
        |> System.Char.ToLower
        |> charToIndex
        |> (+) offset
        |> indexToChar
        |> System.Char.ToUpper
    elif System.Char.IsLower c then
        c
        |> charToIndex
        |> (+) offset
        |> indexToChar
    else c

let rotate shiftKey text = String.map (shiftChar shiftKey) text
