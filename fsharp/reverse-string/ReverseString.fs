﻿module ReverseString
open System

let reverse : string -> string =
    Seq.rev >> String.Concat
