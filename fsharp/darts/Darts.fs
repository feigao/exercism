﻿module Darts

let score (x : double) (y : double) : int =
    let d = x * x + y * y
    if d > 100.0 then 0
    elif d > 25.0 then 1
    elif d > 1.0 then 5
    else 10
