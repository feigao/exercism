﻿module Poker

open System

type Value = int

let readValue (c : Char) : Value =
    match c with
    | 'A' -> 14
    | 'K' -> 13
    | 'Q' -> 12
    | 'J' -> 11
    | c -> int c - int '0'

type Suit =
    | Spade
    | Heart
    | Club
    | Diamond

let readSuit (s : Char) : Suit =
    match s with
    | 'S' -> Spade
    | 'H' -> Heart
    | 'C' -> Club
    | 'D' -> Diamond
    | _ -> failwithf "Unknown suit %c" s

type Card = Card of (Value * Suit)

let readCard (str : string) : Card =
    match str |> Seq.toList with
    | [ '1'; '0'; s ] -> Card(10, readSuit s)
    | [ v; s ] -> Card(readValue v, readSuit s)
    | _ -> failwithf "Unknown card %s" str

type Hand =
    { cards : Card list
      raw : string }

type Category =
    | HighCard of Value list
    | OnePair of Value * (Value list)
    | TwoPairs of Value * Value * Value
    | ThreeOfAKind of Value * (Value list)
    | Straight of Value
    | Flush of Value list
    | FullHouse of Value * Value
    | FourOfAKind of Value * Value
    | StraightFlush of Value

let valuesOfHand hand =
    match hand.cards
          |> List.map (fun (Card(v, s)) -> v)
          |> List.sortDescending with
    | [ 14; 5; 4; 3; 2 ] -> [ 5; 4; 3; 2; 14 ]
    | values -> values

let suitsOfHand (hand : Hand) : Suit list =
    hand.cards |> List.map (fun (Card(_, s)) -> s)

let isFlush hand =
    hand
    |> suitsOfHand
    |> List.distinct
    |> List.length
    |> (=) 1

let allStraight =
    ([ 5; 4; 3; 2; 14 ] :: [ for x in [ 6..14 ] do
                                 yield [ x
                                         x - 1
                                         x - 2
                                         x - 3
                                         x - 4 ] ])

let isStraight hand = List.contains (valuesOfHand hand) allStraight

let categoryOfHand (hand : Hand) : Category =
    if isFlush hand && isStraight hand then
        StraightFlush(valuesOfHand hand |> List.item 0)
    else
        let values = valuesOfHand hand
        match values with
        | [ a; b; c; d; e ] ->
            if a = d then FourOfAKind(a, e)
            elif b = e then FourOfAKind(b, a)
            elif (a = c && d = e) then FullHouse(a, d)
            elif (a = b && c = e) then FullHouse(c, a)
            elif isFlush hand then Flush(valuesOfHand hand)
            elif isStraight hand then Straight(valuesOfHand hand |> List.item 0)
            elif a = c || b = d || c = e then
                ThreeOfAKind(c, values |> List.except [c] |> List.sortDescending)
            elif (a = b && c = d) || (a = b && d = e) || (b = c && d = e) then
                let p1,p2 = b,d
                let r = values |> List.except [p1; p2] |> List.item 0
                TwoPairs (max p1 p2, min p1 p2, r)
            elif a = b || b = c || c = d || d = e then
                let p = (values |> List.sum) - (values |> List.distinct |> List.sum)
                OnePair (p, values |> List.except [p])
            else HighCard values
        | _ -> HighCard values

let bestsOfHands hands =
    let bestCat = hands |> List.map categoryOfHand |> List.max
    hands
    |> List.filter (fun h -> categoryOfHand h = bestCat)

let bestHands (handsStr : string list) : string list =
    handsStr
    |> List.map (fun s ->
           { cards =
                 s.Split(" ")
                 |> Seq.toList
                 |> List.map readCard
             raw = s })
    |> bestsOfHands
    |> List.map (fun h -> h.raw)
