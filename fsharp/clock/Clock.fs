module Clock

type Time =
    { Hour : int
      Minute : int }

let simplify { Hour = h; Minute = m } : Time =
    let time = (h * 60 + m) % (24 * 60)

    let time' =
        if time < 0 then time + 24 * 60
        else time
    { Hour = time' / 60
      Minute = time' % 60 }

let create hours minutes =
    { Hour = hours
      Minute = minutes }
    |> simplify

let add minutes clock =
    { clock with Minute = clock.Minute + minutes } |> simplify
let subtract minutes clock =
    { clock with Minute = clock.Minute - minutes } |> simplify
let clockToString { Hour = h; Minute = m } = sprintf "%02d:%02d" h m

let display clock =
    clock
    |> simplify
    |> clockToString
