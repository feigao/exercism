﻿module Luhn

open System

let valid (number : string) =
    match number
          |> Seq.toList
          |> List.forall (fun c -> Char.IsDigit c || Char.IsWhiteSpace c) with
    | false -> false
    | true ->
        number
        |> Seq.toList
        |> List.filter Char.IsDigit
        |> List.map (fun c -> int c - int '0')
        |> List.rev
        |> List.mapi (fun i v ->
               if i % 2 = 0 then v
               elif v * 2 >= 10 then v * 2 - 9
               else v * 2)
        |> function
        | [ x ] -> false
        | xs ->
            xs
            |> List.sum
            |> fun s -> s % 10 = 0
