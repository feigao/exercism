﻿module Diamond

let make (letter : char) : string =
    if letter = 'A' then "A"
    else
        let len = int letter - int 'A' + 1
        let firstLine =
            String.replicate (len - 1) " " + "A"
            + String.replicate (len - 1) " "
        let mkLine i =
            String.replicate (len - i - 1) " " + string (char (i + 65))
            + String.replicate (2 * i - 1) " " + string (char (i + 65))
            + String.replicate (len - i - 1) " "
        let upper = List.map mkLine [ 1..(len-1) ]
        let lower = List.rev upper |> List.tail
        let body = upper @ lower
        let all = [ firstLine ] @ body @ [ firstLine ]
        String.concat "\n" all
