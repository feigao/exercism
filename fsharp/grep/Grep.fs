﻿module Grep

open System

let matchBuilder args =
    let preprocess =
        if List.contains "-i" args then String.map Char.ToLower
        else id

    let matching : string -> string -> bool =
        fun p l ->
            if List.contains "-x" args then p = l
            else l.Contains(p)

    let reverse =
        if List.contains "-v" args then not
        else id

    fun (pattern : string) (line : string) ->
        matching (preprocess pattern) (preprocess line) |> reverse

type MatchResult =
    { Filename : string
      LineNumber : int
      Line : string }

let printResults (args : string list) showFilename (results : MatchResult list) =
    if List.contains "-l" args then
        results
        |> List.map (fun { Filename = fn } -> fn)
        |> List.distinct
    else
        results
        |> List.map (fun r ->
               [ if showFilename then yield r.Filename
                 if List.contains "-n" args then yield r.LineNumber |> string
                 yield r.Line ]
               |> String.concat ":")

let grepFile matcher pattern filename : MatchResult list =
    System.IO.File.ReadLines filename
    |> Seq.mapi (fun i line ->
           if matcher pattern line then
               Some { Filename = filename
                      LineNumber = i + 1
                      Line = line }
           else None)
    |> Seq.toList
    |> List.collect (Option.toList)

let grep (files : string list) (flagArguments : string list) (pattern : string) =
    let matcher = matchBuilder flagArguments
    files
    |> List.collect (grepFile matcher pattern)
    |> printResults flagArguments (List.length files > 1)
