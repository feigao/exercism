﻿module RailFenceCipher

open System

let flip f a b = f b a

let genCoord (rails : int) (length : int) =
    [ 0..(length - 1) ]
    |> List.mapi (fun i c ->
           let r = i % (2 * rails - 2)
           if r < rails then (r, (c))
           else (2 * rails - 2 - r, (c)))
    |> List.groupBy fst
    |> List.collect (snd >> List.map snd)
    |> flip List.zip [ 0..(length - 1) ]

let encode rails (message : string) =
    let coord =
        message
        |> String.length
        |> genCoord rails
    message
    |> Seq.toList
    |> List.permute (fun i ->
           coord
           |> List.find (fst >> (=) i)
           |> snd)
    |> String.Concat

let decode rails message =
    let coord =
        message
        |> String.length
        |> genCoord rails
    message
    |> Seq.toList
    |> List.permute (fun x -> List.find (fun p -> snd p = x) coord |> fst)
    |> String.Concat
