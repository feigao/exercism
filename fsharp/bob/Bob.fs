﻿module Bob

open System

let sure = "Sure." // question
let chill = "Whoa, chill out!" // yell
let calm = "Calm down, I know what I'm doing!" // yell question
let fine = "Fine. Be that way!" // without actually saying anything
let whatever = "Whatever." // anything else

let response (input : string) : string =
    let str = String.filter (Char.IsWhiteSpace >> not) input
    let letters = String.filter Char.IsLetter str
    if str = "" then fine
    elif letters <> "" && letters |> String.forall Char.IsUpper then
        if str.EndsWith "?" then calm
        else chill
    elif str.EndsWith "?" then sure
    else whatever
