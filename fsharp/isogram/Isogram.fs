﻿module Isogram
open System


let isIsogram (str : string) =
    str.ToCharArray()
    |> Array.filter (Char.IsLetter)
    |> Array.groupBy (Char.ToLower)
    |> Array.map (snd >> Array.length)
    |> Array.forall ((=) 1)
