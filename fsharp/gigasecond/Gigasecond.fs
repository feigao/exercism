﻿module Gigasecond

open System

let gigaseconds = System.TimeSpan.FromSeconds(1000000000.0)
let add (beginDate : DateTime) : DateTime = beginDate + gigaseconds
