﻿module BinarySearch

open System

let find input value =
    let rec aux xs offset =
        match xs with
        | [||] -> None
        | [| x |] ->
            if x = value then Some offset
            else None
        | xs ->
            let len = Array.length xs
            let mid = len / 2
            let midv = Array.item mid xs
            if midv = value then Some(mid + offset)
            elif midv < value then aux (Array.skip mid xs) (offset + mid)
            else aux (Array.take mid xs) offset
    aux input 0
