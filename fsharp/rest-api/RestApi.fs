﻿module RestApi

open Newtonsoft.Json

type URL = string

type Name = string

type Amount = float

type UserDetail =
    { name : Name
      owes : Map<Name, Amount>
      owed_by : Map<Name, Amount>
      balance : Amount }

type DatabaseJson =
    { users : UserDetail list }

type UsernameJson =
    { user : Name }

type UsernameListJson =
    { users : Name list }

type IOUJson =
    { lender : Name
      borrower : Name
      amount : Amount }

type RestApi(database : string) =

    let mutable users =
        JsonConvert.DeserializeObject<DatabaseJson>(database)
        |> (fun d -> d.users)
        |> List.map (fun u -> (u.name, u))
        |> Map.ofList

    let updateBalance user name amount =
        let trans =
            user.owes
            |> Map.fold (fun acc key value -> Map.add key (-value) acc)
                   user.owed_by

        let trans =
            match Map.tryFind name trans with
            | None -> Map.add name amount trans
            | Some value ->
                match value + amount with
                | 0. -> Map.remove name trans
                | x -> Map.add name x trans
        { user with balance = user.balance + amount
                    owed_by = trans |> Map.filter (fun _ v -> v > 0.)
                    owes =
                        trans
                        |> Map.filter (fun _ v -> v < 0.)
                        |> Map.map (fun _ v -> -v) }

    member this.Users() =
        users
        |> Map.toList
        |> List.map snd
        |> List.sort
        |> fun us -> JsonConvert.SerializeObject({ users = us } : DatabaseJson)

    member this.Get(url : URL) =
        match url with
        | "/users" -> this.Users()
        | _ -> failwith "Wrong url"

    member this.Get(url : URL, payload : string) =
        match url with
        | "/users" ->
            JsonConvert.DeserializeObject<UsernameListJson>(payload)
            |> fun x -> x.users
            |> List.map (fun n -> Map.find n users)
            |> fun us ->
                JsonConvert.SerializeObject({ users = us } : DatabaseJson)
        | _ -> failwith "wrong url"

    member this.Post(url : URL, payload : string) =
        match url with
        | "/add" ->
            let user = JsonConvert.DeserializeObject<UsernameJson>(payload)

            let newUser =
                { name = user.user
                  owes = Map.empty
                  owed_by = Map.empty
                  balance = 0.0 }
            users <- Map.add newUser.name newUser users
            JsonConvert.SerializeObject(newUser)
        | "/iou" ->
            let record = JsonConvert.DeserializeObject<IOUJson>(payload)
            let lender = Map.find record.lender users
            let borrower = Map.find record.borrower users
            let newLender = updateBalance lender record.borrower (record.amount)
            let newBorrower =
                updateBalance borrower record.lender (-record.amount)
            users <- Map.add lender.name newLender users
            users <- Map.add borrower.name newBorrower users
            JsonConvert.SerializeObject
                ({ users =
                       [ newLender; newBorrower ]
                       |> List.sortBy (fun u -> u.name) } : DatabaseJson)
        | _ -> failwith "wrong url"
