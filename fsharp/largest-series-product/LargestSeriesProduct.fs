﻿module LargestSeriesProduct

open System

module Seq =
    let tryWindowed windowSize (xs : 'a seq) =
        try
            Seq.windowed windowSize xs |> Some
        with _ -> None

let charToInt =
    function
    | c when '0' <= c && c <= '9' -> int c - int '0' |> Some
    | _ -> None

let largestProduct (input : string) (seriesLength : int) : int option =
    match String.length input, seriesLength with
    | _, 0 -> Some 1
    | _, w when w < 0 -> None
    | l, w when l < w -> None
    | _ ->
        input
        |> Seq.map charToInt
        |> Seq.tryWindowed seriesLength
        |> Option.map (Seq.map (Seq.reduce (Option.map2 (*))))
        |> Option.map (Seq.reduce (Option.map2 max))
        |> Option.flatten
