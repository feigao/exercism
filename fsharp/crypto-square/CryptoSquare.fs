﻿module CryptoSquare

open System

let constant a _ = a

let ciphertext input =
    let normalized =
        input
        |> String.filter System.Char.IsLetterOrDigit
        |> String.map System.Char.ToLower
    match normalized with
    | "" -> ""
    | _ ->
        let c =
            normalized
            |> String.length
            |> float
            |> sqrt
            |> Math.Ceiling
            |> int

        let r = (String.length normalized + c - 1) / c
        let strRect =
            normalized
            + (String.init (r * c - String.length normalized) (constant " "))
        strRect.ToCharArray()
        |> Array.chunkBySize c
        |> Array.transpose
        |> Array.map (String.Concat)
        |> String.concat " "
