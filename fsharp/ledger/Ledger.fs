﻿module Ledger

open System
open System.Globalization

type Entry =
    { dat : DateTime
      des : string
      chg : int }

let mkEntry date description change =
    { dat = DateTime.Parse(date, CultureInfo.InvariantCulture)
      des = description
      chg = change }

let header locale =
    match locale with
    | "en-US" -> "Date       | Description               | Change       "
    | "nl-NL" -> "Datum      | Omschrijving              | Verandering  "
    | _ -> failwith "Known locale"

let printDate locale (dat : DateTime) =
    match locale with
    | "nl-NL" -> dat.ToString("dd-MM-yyyy")
    | "en-US" -> dat.ToString("MM\/dd\/yyyy")
    | _ -> failwith ""

let printDesc _ (des : string) =
    if des.Length <= 25 then des.PadRight(25)
    elif des.Length = 25 then des
    else des.[0..21] + "..."

let (|Negative|NonNegative|) x =
    if x < 0.0 then Negative
    else NonNegative

let printChange (locale : string) currency chg =
    let c = float chg / 100.0
    let cStr = c.ToString("#,#0.00", CultureInfo(locale))
    match c, locale, currency with
    | Negative, "en-US", "USD" -> "($" + cStr.Substring(1) + ")"
    | Negative, "en-US", "EUR" -> "(€" + cStr.Substring(1) + ")"
    | Negative, "nl-NL", "USD" -> "$ " + cStr
    | Negative, "nl-NL", "EUR" -> "€ " + cStr
    | NonNegative, "en-US", "USD" -> "$" + cStr + " "
    | NonNegative, "en-US", "EUR" -> "€" + cStr + " "
    | NonNegative, "nl-NL", "USD" -> "$ " + cStr + " "
    | NonNegative, "nl-NL", "EUR" -> "€ " + cStr + " "
    | _ -> ""
    |> fun s -> s.PadLeft(13)

let formatLedger currency locale entries =
    header locale :: (entries
                      |> List.sortBy (fun x -> x.dat, x.des, x.chg)
                      |> List.map (fun x ->
                             seq {
                                 yield printDate locale x.dat
                                 yield printDesc locale x.des
                                 yield printChange locale currency x.chg
                             }
                             |> String.concat " | "))
    |> String.concat "\n"
