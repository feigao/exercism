module Acronym
  ( abbreviate
  ) where

import           Data.Char
import           Data.List.Split

abbreviate :: String -> String
abbreviate = concatMap wordTrans . splitWhen (not . isAlpha)
  where
    title :: String -> String
    title ""     = ""
    title (x:xs) = toUpper x : xs
    wordTrans :: String -> String
    wordTrans w =
      if all isUpper w'
        then take 1 w'
        else filter isUpper w'
      where
        w' = title w
