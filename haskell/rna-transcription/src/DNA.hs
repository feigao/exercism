module DNA
  ( toRNA
  ) where

toRNA :: String -> Maybe String
toRNA xs = sequence (map complement xs)

complement :: Char -> Maybe Char
complement c =
  case c of
    'G' -> Just 'C'
    'C' -> Just 'G'
    'T' -> Just 'A'
    'A' -> Just 'U'
    _   -> Nothing
