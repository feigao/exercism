module SumOfMultiples (sumOfMultiples) where

import Control.Monad
import Data.List

combinations :: Ord a => [a] -> Int -> [[a]]
combinations xs n = filter ((==n).length.nub) . nub. map sort $ mapM (const xs) [1..n]

sumOfMultiples :: [Integer] -> Integer -> Integer
sumOfMultiples factors limit = sum 
  [ (div ((mul + mul * (div (limit - 1) mul)) * (div (limit - 1) mul)) 2) * ((-1) ^ (n-1))
  | n <- [1..(length factors)]
  , subset <- combinations factors n
  , let mul = foldr1 lcm subset ]

