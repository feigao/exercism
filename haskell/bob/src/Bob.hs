module Bob
  ( responseFor
  ) where

import           Data.Char

responseFor :: String -> String
responseFor s =
  case filter (not . isSpace) s of
    "" -> "Fine. Be that way!"
    xs -> responseFor_ xs

responseFor_ :: String -> String
responseFor_ xs =
  case (isQuestion xs, isYell (filter isAlpha xs)) of
    (True, False)  -> "Sure."
    (False, False) -> "Whatever."
    (True, True)   -> "Calm down, I know what I'm doing!"
    (False, True)  -> "Whoa, chill out!"

isQuestion :: String -> Bool
isQuestion "" = False
isQuestion xs = last xs == '?'

isYell :: String -> Bool
isYell "" = False
isYell xs = all isUpper xs
