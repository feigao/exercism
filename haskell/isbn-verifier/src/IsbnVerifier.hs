module IsbnVerifier (isbn) where

import Data.Char (ord)

validChars :: String
validChars = 'X':['0'..'9']

isbn :: String -> Bool
isbn s = isStringValid s' && isCheckSumValid s' where s' = filter (`elem` validChars) s


isStringValid :: String -> Bool
isStringValid s = all (`elem` tail validChars) (init s) && last s `elem` validChars


isCheckSumValid :: String -> Bool
isCheckSumValid s = checkSum s `mod` 11 == 0


checkSum :: String -> Int
checkSum s = sum (zipWith (*) (strToDigits s) [10,9..1]) where
  strToDigits :: String -> [Int]
  strToDigits = map c2d where
    c2d :: Char -> Int
    c2d 'X' = 10
    c2d c = ord c - ord '0'
